﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace totalfurturestrader
{
    class ReqMgr
    {
        public static void Request_FutureAccountInfo()
        {
//             TrInfo trInfo = new TrInfo();
//             trInfo.sTrCode = "OPW20010";
//             trInfo.sRQName = "Ans_FutureAccountInfo";
//             trInfo.dicInputValue.Add("계좌번호", Global.selectedAccNo);
//             trInfo.dicInputValue.Add("비밀번호", "");
//             trInfo.dicInputValue.Add("비밀번호입력매체구분", "00");
// 
//             int ret = KiwoomManager.Instance.SendTr(trInfo);

            TrInfo trInfo = new TrInfo();
            trInfo.sTrCode = "OPT50031";
            trInfo.sRQName = "Ans_FutureAccountInfo";
            trInfo.dicInputValue.Add("계좌번호", Global.selectedAccNo);

            int ret = KiwoomManager.Instance.SendTr(trInfo);

        }

        public static void Request_FutureAccountInfo2()
        {
            TrInfo trInfo = new TrInfo();
            trInfo.sTrCode = "OPW20010";
            trInfo.sRQName = "Ans_FutureAccountInfo2";
            trInfo.dicInputValue.Add("계좌번호", Global.selectedAccNo);
            trInfo.dicInputValue.Add("비밀번호", "");
            trInfo.dicInputValue.Add("비밀번호입력매체구분", "00");
            
            int ret = KiwoomManager.Instance.SendTr(trInfo);

        }

        public static void Request_HavingFutureStockInfo()
        {
            TrInfo trInfo = new TrInfo();
            trInfo.sTrCode = "OPT50027";
            trInfo.sRQName = "Ans_HavingFutureStockInfo";
            trInfo.dicInputValue.Add("계좌번호", Global.selectedAccNo);

            int ret = KiwoomManager.Instance.SendTr(trInfo);
            //ProcMgr.Instance.AddProc(KiwoomManager.Instance.SendTr, trInfo);
        }

        public static void Request_OrderingInfo()
        {
            TrInfo trInfo = new TrInfo();

            trInfo.sRQName = "Ans_OrderingInfo";

            trInfo.sTrCode = "OPT50026";
            trInfo.dicInputValue.Add("종목코드", "");
            trInfo.dicInputValue.Add("조회구분", "1");
            trInfo.dicInputValue.Add("매매구분", "0");
            trInfo.dicInputValue.Add("체결구분", "1");
            trInfo.dicInputValue.Add("계좌번호", Global.selectedAccNo);

            KiwoomManager.Instance.SendTr(trInfo);
            //ProcMgr.Instance.AddProc(KiwoomManager.Instance.SendTr, trInfo);
        }

        public static void Request_NewOrder_Sell(string screenName_,int num_)
        {
            var req = new FoOrderInfo();
            req.sRQName = screenName_;
            req.sAccNo = Global.selectedAccNo;
            req.nOrderType = 1;
            req.sCode = Global.comboBox_KospiCode;
            req.nQty = num_;
            req.sSlbyTp = "1";
            req.sHogaGb = "3";
            req.sPrice = "";
            req.sOrgOrderNo = "";

            KiwoomManager.Instance.SendOrder(req);
        }

        public static void Request_NewOrder_Buy(string screenName_, int num_)
        {
            var req = new FoOrderInfo();
            req.sRQName = screenName_;
            req.sAccNo = Global.selectedAccNo;
            req.nOrderType = 1;
            req.sCode = Global.comboBox_KospiCode;
            req.nQty = num_;
            req.sSlbyTp = "2";
            req.sHogaGb = "3";
            req.sPrice = "";
            req.sOrgOrderNo = "";

            KiwoomManager.Instance.SendOrder(req);
        }

        public static int Request_CancelOrder(string screenName_, int num_, string orderNum_)
        {
            var req = new FoOrderInfo();
            req.sRQName = screenName_;
            req.sAccNo = Global.selectedAccNo;
            req.nOrderType = 3;
            req.sCode = Global.comboBox_KospiCode;
            req.nQty = num_;
            req.sSlbyTp = "";
            req.sHogaGb = "";
            req.sPrice = "";
            req.sOrgOrderNo = orderNum_;

            return KiwoomManager.Instance.SendOrder(req);
        }
    }
}
