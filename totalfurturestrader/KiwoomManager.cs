﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using AxKHOpenAPILib;
using System.Reflection;

public class TrInfo
{
    public string sRQName;
    public string sTrCode;
    public int nPrevNext = 0;

    public Dictionary<string, string> dicInputValue = new Dictionary<string, string>();
}

public class OrderInfo
{
    public string sRQName;
    public string sAccNo;
    public int nOrderType;
    public string sCode;
    public int nQty;
    public int nPrice;
    public string sHogaGb;
    public string sOrgOrderNo;
}

public class FoOrderInfo : OrderInfo
{
    public string sSlbyTp;
    public string sPrice;
}

class KiwoomManager:Singleton<KiwoomManager>
{
    public List<object> listEventReceiver = new List<object>();

    public Dictionary<string, int> screenNoDic = new Dictionary<string, int>();
    public Dictionary<string, string> dicPreStockPrice = new Dictionary<string, string>();
    public Dictionary<string, string> dicStockPrice = new Dictionary<string, string>();

    public void Init()
    {
        AttachCallbackEvent("OnEventConnect");
        AttachCallbackEvent("OnReceiveConditionVer");
        AttachCallbackEvent("OnReceiveTrCondition");
        AttachCallbackEvent("OnReceiveRealCondition");
        AttachCallbackEvent("OnReceiveTrData");
        AttachCallbackEvent("OnReceiveRealData");
        AttachCallbackEvent("OnReceiveChejanData");
        AttachCallbackEvent("OnReceiveMsg");
    }

    void AttachCallbackEvent(string strNameEvent_)
    {
        foreach(object receiver in listEventReceiver)
        {
            MethodInfo func = receiver.GetType().GetMethod(strNameEvent_ , BindingFlags.Public | BindingFlags.Instance);
            if (func == null)
                continue;

            EventInfo eventInfo = KiwoomAPI.Get.GetType().GetEvent(strNameEvent_);
            Type type = eventInfo.EventHandlerType;
            Delegate action = Delegate.CreateDelegate(type, receiver, func);

            eventInfo.AddEventHandler(KiwoomAPI.Get , action);
          
        }
    }

    public int GetScreenNo(string screenName_)
    {
        if (screenNoDic.ContainsKey(screenName_) == false)
        {
            screenNoDic.Add(screenName_, 5000 + screenNoDic.Count);
        }

        return screenNoDic[screenName_];
    }

    public string GetScreenName(int num_)
    {
        if (screenNoDic.ContainsValue(num_) == false)
            return "";

       return screenNoDic.FirstOrDefault(x => x.Value == num_).Key;
    }

    public void ExitEvent()
    {
        foreach (KeyValuePair<string, int> item in screenNoDic)
            KiwoomAPI.Get.DisconnectRealData(item.Value.ToString());

        KiwoomAPI.Get.SetRealRemove("ALL", "ALL");
    }

    public int SendTr(object info_)
    {
        TrInfo tr = info_ as TrInfo;

        foreach (KeyValuePair<string, string> item in tr.dicInputValue)
            KiwoomAPI.Get.SetInputValue(item.Key, item.Value);

        if (Global.logViewer != null)
            Global.logViewer.Print_CommonLog("▶" + tr.sRQName + " >> sTrCode : " + tr.sTrCode);

        return KiwoomAPI.Get.CommRqData(tr.sRQName, tr.sTrCode, tr.nPrevNext, GetScreenNo(tr.sRQName).ToString());
    }

    public int SendOrder(object info_)
    {      
        var foInfo = info_ as FoOrderInfo;
        if (foInfo == null)
        {
            OrderInfo order = info_ as OrderInfo;
            return KiwoomAPI.Get.SendOrder(order.sRQName, GetScreenNo(order.sRQName).ToString()
                    , order.sAccNo, order.nOrderType, order.sCode
                    , order.nQty, order.nPrice, order.sHogaGb, order.sOrgOrderNo);
        }
        else
        {
            return KiwoomAPI.Get.SendOrderFO(foInfo.sRQName, GetScreenNo(foInfo.sRQName).ToString()
                    , foInfo.sAccNo, foInfo.sCode, foInfo.nOrderType, foInfo.sSlbyTp
                    , foInfo.sHogaGb, foInfo.nQty, foInfo.sPrice, foInfo.sOrgOrderNo);
        }
    }

    public int RegRealData(string screenName_, List<string> listCode_)
    {
        string strCodeList = "";
        for (int i = 0; i < listCode_.Count; ++i)
        {
            dicPreStockPrice[listCode_[i]] = "0";
            dicStockPrice[listCode_[i]] = "0";

            strCodeList += listCode_[i];
            if (i != (listCode_.Count - 1))
                strCodeList += ';';
        }

        return KiwoomAPI.Get.SetRealReg(GetScreenNo(screenName_).ToString(), strCodeList, "10;11;12;20;21;41;42;55;75;141;161;151;990;991;8019;9201;252;253;257", "0");
    }
}

