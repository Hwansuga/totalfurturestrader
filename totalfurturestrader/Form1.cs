﻿using AxKHOpenAPILib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace totalfurturestrader
{
    public partial class Form1 : Form
    {
        LogViewer logViewer = null;

        ProcScript procScript = new ProcScript();

        private object lockObject = new object();
        private object lockObjectPrice = new object();

        public Form1()
        {
            InitializeComponent();

            FormClosing += ExitEvent;
        }

        public void Print_Log(string msg_)
        {
            listBox_Log.Invoke((MethodInvoker)delegate
            {
                Util.AutoLineStringAdd(listBox_Log, msg_);
            });
        }

        void UpdateRamainTime(string msg_)
        {
            this.Invoke(new MethodInvoker(delegate() {
                label_RemainTime.Text = msg_;
            }));
        }

        void UpdateStandardPrice(string msg_)
        {
            label_StandardPrice.Invoke(new MethodInvoker(delegate ()
            {
                label_StandardPrice.Text = msg_;
            }));
        }

        void UpdateHaveMoney(string msg_)
        {
#if DEBUG
            Print_Log($"Change money : {msg_}");
#endif

            label_HaveMoney.Invoke(new MethodInvoker(delegate ()
            {
                label_HaveMoney.Text = msg_;
            }));
        }

        void UpdateTodayWinMoney(string msg_)
        {
            if (string.IsNullOrEmpty(msg_))
                return;

            double ret = 0.0f;
            string input = msg_;
            if (double.TryParse(msg_, out ret))
            {
                Global.curTodayWinmoney = ret;
                input = ret.ToString();
            }
                

            label_TodayWinMoney.Invoke(new MethodInvoker(delegate ()
            {
                label_TodayWinMoney.Text = input;
                if (msg_.Contains("-"))
                    label_TodayWinMoney.ForeColor = Color.Blue;
                else
                    label_TodayWinMoney.ForeColor = Color.Red;
            }));
        }

        void UpdatePurchaseDiff(string msg_)
        {
            if (string.IsNullOrEmpty(msg_))
                return;

            label_PurchaseDiff.Invoke(new MethodInvoker(delegate ()
            {
                label_PurchaseDiff.Text = msg_;
                if (msg_.Contains("-"))
                    label_PurchaseDiff.ForeColor = Color.Blue;
                else
                    label_PurchaseDiff.ForeColor = Color.Red;
            }));
        }
        void UpdateCleanDiff(string msg_)
        {
            if (string.IsNullOrEmpty(msg_))
                return;

            label_ClearDiff.Invoke(new MethodInvoker(delegate ()
            {
                label_ClearDiff.Text = msg_;
                if (msg_.Contains("-"))
                    label_ClearDiff.ForeColor = Color.Blue;
                else
                    label_ClearDiff.ForeColor = Color.Red;
            }));
        }

        void UpdateLoseDiff(string msg_)
        {
            if (string.IsNullOrEmpty(msg_))
                return;

            label_LoseDiff.Invoke(new MethodInvoker(delegate ()
            {
                label_LoseDiff.Text = msg_;
                if (msg_.Contains("-"))
                    label_LoseDiff.ForeColor = Color.Blue;
                else
                    label_LoseDiff.ForeColor = Color.Red;
            }));
        }

        void UpdateWinMoney(string msg_)
        {
            if (string.IsNullOrEmpty(msg_))
                return;

            double ret = 0.0f;
            string input = msg_;
            if (double.TryParse(msg_, out ret))
                input = ret.ToString();

            label_WinMoney.Invoke(new MethodInvoker(delegate ()
            {
                label_WinMoney.Text = input;
            }));
            if (msg_.Contains("-"))
                label_WinMoney.ForeColor = Color.Blue;
            else
                label_WinMoney.ForeColor = Color.Red;
        }

        public void UpdatePrice(string msg_)
        {
            listBox_Price.Invoke(new MethodInvoker(delegate ()
            {
                var itemStamp = DateTime.Now.ToString("HH:mm:ss");
                listBox_Price.Items.Insert(0,$"{itemStamp} : {msg_}");
            }));
        }

        void ExitEvent(object sender, FormClosingEventArgs e)
        {
            var curSetting = new Setting();
            Util.CopyValue(Global.Instance, curSetting);
            var jObj = Newtonsoft.Json.JsonConvert.SerializeObject(curSetting);
            File.WriteAllText("CurSetting.json", jObj);

            KiwoomManager.Instance.ExitEvent();
          
            Application.ExitThread();
            Environment.Exit(0);
        }

        void SetStartPosition()
        {
            StartPosition = FormStartPosition.Manual;
            Rectangle res = Screen.PrimaryScreen.Bounds;
            Location = new Point(res.Width - Size.Width, 0);
        }

        bool SetCurSetting()
        {
            if (File.Exists("CurSetting.json") == false)
                return false;

            var data = File.ReadAllText("CurSetting.json");
            var curSetting = Newtonsoft.Json.JsonConvert.DeserializeObject<Setting>(data);
            Util.CopyValue(curSetting, Global.Instance);
            return true;
        }

        void InitChart()
        {
            Series ser_price = chart_Price.Series.Add("현재가");
            ser_price.ChartType = SeriesChartType.Candlestick;
            ser_price["PriceUpColor"] = "Red";
            ser_price["PriceDownColor"] = "Blue";
            ser_price.XValueMember = "date";
            ser_price.YValueMembers = "high, low, open, close";
            ser_price.XValueType = ChartValueType.Date;
            ser_price["ShowOpenClose"] = "Both";
            
            chart_Price.DataManipulator.IsStartFromFirst = true;
            chart_Price.ChartAreas[0].AxisY.Minimum = 314;

            chart_Price.ChartAreas[0].CursorX.AutoScroll = true;
            chart_Price.ChartAreas[0].CursorY.AutoScroll = true;
            chart_Price.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
            chart_Price.ChartAreas[0].CursorY.IsUserSelectionEnabled = true;
            //chart_Price.ChartAreas[0].AxisX.IsReversed = true;
            chart_Price.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            chart_Price.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;

            chart_Price.DataSource = Global.listHistoryPrice;
            chart_Price.DataBind();
        }

        private void chart_Price_MouseMove(object sender, MouseEventArgs e)
        {
            Point mousePoint = new Point(e.X, e.Y);
            chart_Price.ChartAreas[0].CursorX.SetCursorPixelPosition(mousePoint, true);
            chart_Price.ChartAreas[0].CursorY.SetCursorPixelPosition(mousePoint, true);

            var pos = e.Location;
            if (pos == prevPosition)
                return;
            prevPosition = pos;
            var results = chart_Price.HitTest(pos.X, pos.Y, false, ChartElementType.DataPoint);

            foreach (var result in results)
            {
                if (result.ChartElementType == ChartElementType.DataPoint) // set ChartElementType.PlottingArea for full area, not only DataPoints
                {
                    DataPoint dp = result.Series.Points[result.PointIndex];
                    string msg = $"High={dp.YValues[0]}\nLow={dp.YValues[1]}\nOpen={dp.YValues[2]}\nClose={dp.YValues[3]}";
                    tooltip.Show(msg, chart_Price, pos.X, pos.Y - 15);
                }
            }

        }


        private void Form1_Load(object sender, EventArgs e)
        {
            EventUtil.ConnectCustomEvent(this);
            if(this.SetCurSetting())
            {
                try
                {
                    EventUtil.UpdateUIFromGlobal(this, new List<string> { "comboBox_Account", "comboBox_KospiCode" });
                }
                catch
                {
                    File.Delete("CurSetting.json");
                    Global.UpdateValueFrom(this);
                }            
            }
            else
            {
                Global.UpdateValueFrom(this);
            }
                

            Global.uiController = this;            
            this.SetStartPosition();
#if DEBUG
            logViewer = new LogViewer();
            Global.logViewer = logViewer;
            logViewer.Show();
            Util.SetFormPos(this, logViewer , FORMPOS.FROMPOS_LEFT);
#endif

            KiwoomManager.Instance.listEventReceiver.Add(this);
#if DEBUG
            KiwoomManager.Instance.listEventReceiver.Add(logViewer);
#endif
            KiwoomManager.Instance.Init();

//             var listProcView = new Thread(() =>
//             {
//                 while (true)
//                 {
//                     var listName = procScript.GetJobsName();
//                     this.Invoke(new MethodInvoker(delegate ()
//                     {
//                         listBox_Proc.DataSource = null;
//                         listBox_Proc.DataSource = listName;
//                         listBox_Proc.Refresh();
//                     }));                   
//                     Thread.Sleep(1000);
//                 }
//             });
//             listProcView.Start();

#if DEBUG
            button_Test.Visible = true;
#else
            button_Test.Visible = false;
#endif

            InitChart();

            //TestChart();
        }

        void TestChart()
        {
            Random r = new Random(Environment.TickCount);
            for (int i=0; i<20; i++)
            {                    
                var temp = new PriceEntityObject();
                temp.date = DateTime.Now.AddSeconds(i).ToString("HHmmss");
                temp.open = r.Next(300, 330);
                temp.close = r.Next(300, 330);
                temp.low = r.Next(300, 310);
                temp.high = r.Next(310, 330);
                Global.listHistoryPrice.Add(temp);
            }

            chart_Price.DataBind();
        }

        void SetLoginInfo()
        {
            List<string> listAccount = KiwoomAPI.Get.GetLoginInfo("ACCLIST").Trim().Split(';').ToList();
            comboBox_Account.Items.Clear();
            foreach (string item in listAccount)
                comboBox_Account.Items.Add(item);

            if (comboBox_Account.Items.Count > 0)
            {
#if DEBUG
                comboBox_Account.SelectedIndex = 1;
#else
                comboBox_Account.SelectedIndex = 0;
#endif
            }

            label_UserName.Text = KiwoomAPI.Get.GetLoginInfo("USER_NAME");
            label_UserID.Text = KiwoomAPI.Get.GetLoginInfo("USER_ID");

            if (KiwoomAPI.Get.GetLoginInfo("GetServerGubun") == "1")
            {
                label_Server.Text = "모의서버";
            }
            else
            {
                label_Server.Text = "실제서버 " + KiwoomAPI.Get.GetLoginInfo("GetServerGubun");
            }

            button_CheckAcc.Enabled = true;
            
            SetStockListInfo();
        }

        public void OnEventConnect(object sender, _DKHOpenAPIEvents_OnEventConnectEvent e)
        {
            if (e.nErrCode == 0)//if문으로 e.nErrCode가 0인지 검사해서 로그인이 잘 되었는지 체크합니다
            {
                SetLoginInfo();

                KiwoomAPI.Get.KOA_Functions("ShowAccountWindow", string.Empty);
            }
        }

        void SetStockListInfo()
        {
            List<string> listCode = KiwoomAPI.Get.GetFutureList().Split(';').ToList();
//            listCode = listCode.FindAll(x => x.IndexOf("101") == 0);
            comboBox_KospiCode.Items.Clear();

//             //한종목만 가져오게 설정
            listCode = listCode.GetRange(0, 1);

            foreach (string item in listCode)
            {
                //string stockName = KiwoomAPI.Get.GetMasterCodeName(item);
                comboBox_KospiCode.Items.Add(item);
            }

            if (comboBox_KospiCode.Items.Count > 0)
            {
                comboBox_KospiCode.SelectedIndex = 0;
            }
        }

        public void OnReceiveTrData(object sender, _DKHOpenAPIEvents_OnReceiveTrDataEvent e)
        {
            MethodInfo func = this.GetType().GetMethod(e.sRQName, BindingFlags.NonPublic | BindingFlags.Instance);
            if (func == null)
                return;

            object[] prams = new object[1];
            prams[0] = e;
            func.Invoke(this, prams);
        }
        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            KiwoomAPI.Get.CommConnect();
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            KiwoomAPI.Get.CommTerminate();
        }

        private void button_CheckAcc_Click(object sender, EventArgs e)
        {
            if (KiwoomAPI.Get.GetConnectState() == 0)
            {
                MessageBox.Show("로그인을 해 주세요");
                return;
            }

            button_Start.Enabled = true;
#if !DEBUG
            button_CheckAcc.Enabled = false;
#endif

            ReqMgr.Request_FutureAccountInfo();
            ReqMgr.Request_FutureAccountInfo2();
            ReqMgr.Request_HavingFutureStockInfo();
            ReqMgr.Request_OrderingInfo();
        }

        void Ans_FutureAccountInfo(_DKHOpenAPIEvents_OnReceiveTrDataEvent e)
        {
            string data = KiwoomAPI.Get.GetCommData(e.sTrCode, e.sRQName, 0, "손익");
            this.UpdateWinMoney(data);

            data = KiwoomAPI.Get.GetCommData(e.sTrCode, e.sRQName, 0, "당일매도손익");
            this.UpdateTodayWinMoney(data);
          

            //             List <JobInfo> listGetData = new List<JobInfo>();
            //             listGetData.Add(new JobInfo() { dataID = "당일매도손익" });
            //             listGetData.Add(new JobInfo() { dataID = "손익" });
            //             listGetData.Add(new JobInfo() { dataID = "매입단가" });
            //             listGetData.Add(new JobInfo() { dataID = "총매입가" });
            //             listGetData.Add(new JobInfo() { dataID = "평가금액" });
            //             listGetData.Add(new JobInfo() { dataID = "예탁총액" } );
            //             listGetData.Add(new JobInfo() { dataID = "예탁현금" } );
            //             listGetData.Add(new JobInfo() { dataID = "증거금총액" });
            //             listGetData.Add(new JobInfo() { dataID = "증거금현금" });
            //             listGetData.Add(new JobInfo() { dataID = "증거금대용금" });
            //             listGetData.Add(new JobInfo() { dataID = "주문가능총액" });
            //             listGetData.Add(new JobInfo() { dataID = "주문가능현금" });
            //             listGetData.Add(new JobInfo() { dataID = "주문가대용금" });
            //             listGetData.Add(new JobInfo() { dataID = "순자산금액" });
            // 
            //             listGetData.Add(new JobInfo() { dataID = "선물평가손익" });


            //             foreach (var item in listGetData)
            //             {
            //                 string data = KiwoomAPI.Get.GetCommData(e.sTrCode, e.sRQName, 0, item.dataID);
            //                 if (string.IsNullOrEmpty(data))
            //                     data = "0";
            // 
            //                 double ret = 0.0f;
            //                 if (double.TryParse(data.Trim(), out ret))
            //                     item.dataInfo = ret.ToString();
            //                 else
            //                     item.dataInfo = data.Trim();
            //                 //item.dataInfo = long.Parse(data).ToString("n0");
            //             }
            //             dataGridView_AccountInfo.DataSource = null;
            //             dataGridView_AccountInfo.DataSource = listGetData;
            //             dataGridView_AccountInfo.Refresh();
        }

        void Ans_FutureAccountInfo2(_DKHOpenAPIEvents_OnReceiveTrDataEvent e)
        {
            string data = KiwoomAPI.Get.GetCommData(e.sTrCode, e.sRQName, 0, "예탁총액");
            double ret = 0.0f;
            if (string.IsNullOrEmpty(data) == false)
            {
                if (double.TryParse(data, out ret))
                    this.UpdateHaveMoney(ret.ToString());
                else
                    this.UpdateHaveMoney(data);
            }               
        }

        private void button_TraceStock_Click(object sender, EventArgs e)
        {
            if (KiwoomAPI.Get.GetConnectState() == 0)
            {
                MessageBox.Show("로그인을 해 주세요");
                return;
            }

            List<string> listItem = new List<string>();
            listItem.Add(comboBox_KospiCode.SelectedItem.ToString());

            int ret = KiwoomManager.Instance.RegRealData("선물시세", listItem);
            if (ret == 0)
            {
                if (logViewer != null)
                    logViewer.Print_CommonLog("선물시세 요청 성공");
                button_TraceStock.Enabled = false;
            }
            else
            {
                if (logViewer != null)
                    logViewer.Print_CommonLog("선물시세 요청 실패");
            }
        }

        void CheckWinMoney()
        {
            long value = 0;
            foreach (StockInfo item in Global.listStockInfo)
            {
                int cnt = int.Parse(item.보유수량);
                double itemPrice = double.Parse(item.매입단가.Replace("-", ""));
                double gap = itemPrice - Math.Round(Global.curStockPrice, 3);
                double tex = Math.Round(itemPrice * 15 * cnt);
                if (item.매매구분.Contains("2"))
                {
                    value -= (long)((gap / Global.oneDigit * cnt) * Global.valuePerDigit) / 100 * 100;
                }
                else
                {
                    value += (long)((gap / Global.oneDigit * cnt) * Global.valuePerDigit) / 100 * 100;
                }
                value -= (long)tex;
            }

            this.UpdateWinMoney(value.ToString());
        }
       
        public void OnReceiveRealData(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveRealDataEvent e)
        {
            //lock (this.lockObjectPrice)
            {
                string value = KiwoomAPI.Get.GetCommRealData(comboBox_KospiCode.SelectedItem.ToString(), 10);
                if (string.IsNullOrEmpty(value))
                {
                    if (Global.curStockPrice > 0.0f)
                    {
                        var dateTIme = DateTime.Now.ToString("HHmmss");
                        Global.AddHistoryData(dateTIme, Global.curStockPrice);

                        chart_Price.DataSource = null;
                        chart_Price.DataSource = Global.listHistoryPrice;
                        chart_Price.DataBind();
                    }

                    return;
                }
                else
                {
                    Global.curStockPrice = double.Parse(value.Trim().Replace("-", "").Replace("+", ""));
                    var dateTIme = DateTime.Now.ToString("HHmmss");

                    if (chart_Price.ChartAreas[0].AxisY.Minimum > Global.curStockPrice)
                    {
                        chart_Price.ChartAreas[0].AxisY.Minimum = Global.curStockPrice;
                    }
                    


                    if (Global.AddHistoryData(dateTIme, Global.curStockPrice)) {
                        this.CheckWinMoney();
                    }
                    chart_Price.DataSource = null;
                    chart_Price.DataSource = Global.listHistoryPrice;
                    chart_Price.DataBind();

                    label_KospiPrice.Invoke(new MethodInvoker(delegate ()
                    {
                        label_KospiPrice.Text = Global.curStockPrice.ToString("F2");
                        if (value.Contains("+"))
                            label_KospiPrice.ForeColor = Color.Red;
                        else if (value.Contains("-"))
                            label_KospiPrice.ForeColor = Color.Blue;
                        else
                            label_KospiPrice.ForeColor = Color.Black;

                    }));
                }
            }
                
            
            var 주문가능수량 = KiwoomAPI.Get.GetCommRealData(comboBox_KospiCode.SelectedItem.ToString(), 933);
            var 당일실현손익 = KiwoomAPI.Get.GetCommRealData(comboBox_KospiCode.SelectedItem.ToString(), 990);
            var 손익률 = KiwoomAPI.Get.GetCommRealData(comboBox_KospiCode.SelectedItem.ToString(), 8019);
            var 예수금 = KiwoomAPI.Get.GetCommRealData(comboBox_KospiCode.SelectedItem.ToString(), 951);
            var 선물매도금액합계 = KiwoomAPI.Get.GetCommRealData(comboBox_KospiCode.SelectedItem.ToString(), 13011);
            var 선물매도평가금액합계 = KiwoomAPI.Get.GetCommRealData(comboBox_KospiCode.SelectedItem.ToString(), 13012);
            var 선물옵션손익합계 = KiwoomAPI.Get.GetCommRealData(comboBox_KospiCode.SelectedItem.ToString(), 13018);
        }

        public void OnReceiveMsg(object sender, _DKHOpenAPIEvents_OnReceiveMsgEvent e)
        {
#if DEBUG
            //Print_Log($"◀OnReceiveMsg << { KiwoomManager.Instance.GetScreenName(int.Parse(e.sScrNo)) } / {e.sMsg} / {e.sTrCode}");
#endif
        }

        public void OnReceiveChejanData(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveChejanDataEvent e)
        {
            //lock (lockObject)
            {
                if (e.sGubun == "0")
                {
#if DEBUG
                    //List<int> listFid = e.sFIdList.Split(';').ToList().ConvertAll(o => int.Parse(o));
//                     Print_Log($"주문체결통보 - 주문번호 : {KiwoomAPI.Get.GetChejanData(9203)} " +
//                         $"/ 종목코드 : {KiwoomAPI.Get.GetChejanData(9001)} " +
//                         $"/ 주문상태 : {KiwoomAPI.Get.GetChejanData(913)} " +
//                         $"/ 종목명 : {KiwoomAPI.Get.GetChejanData(302)} " +
//                         $"/ 주문수량 : {KiwoomAPI.Get.GetChejanData(900)} " +
//                         $"/ 미체결수량 : {KiwoomAPI.Get.GetChejanData(902)} " +
//                         $"/ 주문구분 : {KiwoomAPI.Get.GetChejanData(905)} " +
//                         $"/ 매매구분 : {KiwoomAPI.Get.GetChejanData(906)} " +
//                         $"/ 체결량 : {KiwoomAPI.Get.GetChejanData(911)}");
#endif

                    var 미체결수량 = int.Parse(KiwoomAPI.Get.GetChejanData(902));
                    var order = Global.listOrderIfo.Find(x => x.주문번호 == KiwoomAPI.Get.GetChejanData(9203));
                    if (order == null)
                    {

                        if (미체결수량 > 0)
                        {
                            var newOrder = new OrderProcessInfo();
                            newOrder.종목코드 = KiwoomAPI.Get.GetChejanData(9001);
                            newOrder.주문구분 = KiwoomAPI.Get.GetChejanData(905);
                            newOrder.주문수량 = KiwoomAPI.Get.GetChejanData(902);
                            newOrder.주문번호 = KiwoomAPI.Get.GetChejanData(9203);
                            newOrder.주문가격 = double.Parse(KiwoomAPI.Get.GetChejanData(901)).ToString();
                            Global.listOrderIfo.Add(newOrder);
                        }
                    }
                    else
                    {
                        if (미체결수량 > 0)
                        {
                            order.주문수량 = KiwoomAPI.Get.GetChejanData(902);
                        }
                        else
                        {
                            Global.listOrderIfo.Remove(order);
                        }

                    }

                    dataGridView_OderInfo.DataSource = null;
                    dataGridView_OderInfo.DataSource = Global.listOrderIfo;
                    dataGridView_OderInfo.Refresh();
                }
                else if (e.sGubun == "4")
                {
                    //ReqMgr.Request_FutureAccountInfo();
#if DEBUG
                    Print_Log($"잔고 - 종목코드 : {KiwoomAPI.Get.GetChejanData(9001)} " +
                        $"보유수량 : ${KiwoomAPI.Get.GetChejanData(930)} " +
                        $"매도/매수구분 : ${KiwoomAPI.Get.GetChejanData(946)}");
#endif

                    var 매도매수구분 = KiwoomAPI.Get.GetChejanData(946);
                    var 보유수량 = int.Parse(KiwoomAPI.Get.GetChejanData(930));

                    var stock = Global.listStockInfo.Find(x => x.종목코드 == KiwoomAPI.Get.GetChejanData(9001));
                    if (stock == null)
                    {

                        if (보유수량 > 0)
                        {
                            var newStock = new StockInfo();
                            newStock.종목코드 = KiwoomAPI.Get.GetChejanData(9001);
                            newStock.종목명 = KiwoomAPI.Get.GetChejanData(302);

                            if (매도매수구분.Contains("2"))
                                newStock.매매구분 = "2";
                            else
                                newStock.매매구분 = "1";

                            newStock.보유수량 = KiwoomAPI.Get.GetChejanData(930);
                            newStock.매입단가 = double.Parse(KiwoomAPI.Get.GetChejanData(931)).ToString();
                            Global.listStockInfo.Add(newStock);
                        }
                    }
                    else
                    {
                        if (보유수량 > 0)
                        {
                            if (매도매수구분.Contains("2"))
                                stock.매매구분 = "2";
                            else
                                stock.매매구분 = "1";
                            stock.보유수량 = KiwoomAPI.Get.GetChejanData(930);
                            stock.매입단가 = double.Parse( KiwoomAPI.Get.GetChejanData(931)).ToString();
                        }
                        else
                        {
                            Global.listStockInfo.Remove(stock);
                        }

                    }

                    var 당일총매도손일 = KiwoomAPI.Get.GetChejanData(950);
                    var 당일실현손익유가 = KiwoomAPI.Get.GetChejanData(990);
                    var 당일실현손익신용 = KiwoomAPI.Get.GetChejanData(992);
                    var 손익률 = KiwoomAPI.Get.GetChejanData(8019);

                    ReqMgr.Request_FutureAccountInfo();

                    this.UpdateHavingStockInfo();
                }
            }
        }

        private void comboBox_Account_SelectedIndexChanged(object sender, EventArgs e)
        {
            Global.selectedAccNo = comboBox_Account.SelectedItem.ToString();
        }

        void Ans_HavingFutureStockInfo(_DKHOpenAPIEvents_OnReceiveTrDataEvent e)
        {
            //lock (this.lockObject)
            {
                Global.listStockInfo.Clear();

                int cnt = KiwoomAPI.Get.GetRepeatCnt(e.sTrCode, e.sRQName);
                if (logViewer != null)
                    logViewer.Print_CommonLog($"Ans_HavingFutureStockInfo cnt : {cnt}");

                for (int i = 0; i < cnt; ++i)
                {
                    var have = KiwoomAPI.Get.GetCommData(e.sTrCode, e.sRQName, i, "보유수량").Trim();
                    if (int.Parse(have) <= 0)
                        continue;

                    StockInfo stockInfo = new StockInfo();

                    var listFields = stockInfo.GetType().GetProperties().ToList();
                    foreach (var item in listFields)
                    {
                        var value = KiwoomAPI.Get.GetCommData(e.sTrCode, e.sRQName, i, item.Name).Trim();
                        double dValue = -0.01;
                        bool dRet = double.TryParse(value, out dValue);
                        if (dRet)
                            item.SetValue(stockInfo, dValue.ToString());
                        else
                            item.SetValue(stockInfo, value);

                    }
                    Global.listStockInfo.Add(stockInfo);
                }

                this.UpdateHavingStockInfo();
                
            }
            
        }

        void UpdateHavingStockInfo()
        {
            dataGridView_HavingStockInfo.DataSource = null;
            dataGridView_HavingStockInfo.DataSource = Global.listStockInfo;
            dataGridView_HavingStockInfo.Refresh();

// 
//             int culIndex = dataGridView_HavingStockInfo.Columns["매매구분"].Index;
//             foreach (DataGridViewRow row in dataGridView_HavingStockInfo.Rows)
//             {
//                 var data = row.Cells[culIndex].Value.ToString().Trim();
//                 if (data == "1")
//                     row.Cells[culIndex].Value = "매도";
//                 else if (data == "2")
//                     row.Cells[culIndex].Value = "매수";
//             }
        }

        void Ans_OrderingInfo(_DKHOpenAPIEvents_OnReceiveTrDataEvent e)
        {
            //lock (this.lockObject)
            {
                Global.listOrderIfo.Clear();
                int cnt = KiwoomAPI.Get.GetRepeatCnt(e.sTrCode, e.sRQName);
                if (logViewer != null)
                    logViewer.Print_CommonLog($"Ans_Orderingnfo cnt : {cnt}");
                for (int i = 0; i < cnt; ++i)
                {
                    if (int.Parse(KiwoomAPI.Get.GetCommData(e.sTrCode, e.sRQName, i, "주문수량").Trim()) <= 0)
                        continue;

                    OrderProcessInfo stockInfo = new OrderProcessInfo();

                    var listFields = stockInfo.GetType().GetProperties().ToList();
                    foreach (var item in listFields)
                    {
                        var value = KiwoomAPI.Get.GetCommData(e.sTrCode, e.sRQName, i, item.Name).Trim();
                        double dValue = -0.01;
                        bool dRet = double.TryParse(value, out dValue);
                        if (dRet)
                            item.SetValue(stockInfo, dValue.ToString());
                        else
                            item.SetValue(stockInfo, value);

                    }
                    Global.listOrderIfo.Add(stockInfo);
                }

                dataGridView_OderInfo.DataSource = null;
                dataGridView_OderInfo.DataSource = Global.listOrderIfo;
                dataGridView_OderInfo.Refresh();
            }
            
        }

        bool IsValidStateToStart()
        {
            if (Global.curStockPrice <= 0.0f)
            {
                MessageBox.Show($"최근 가격 수집중입니다.");
                return false;
            }
                

            if (Global.checkBox_BuyRight == false && Global.checkBox_SellRight == false)
            {
                MessageBox.Show($"매수 또는 매도 매매 체크박스를 선택해 주세요");
                return false;
            }
                

//             var preDate = DateTime.Now.AddSeconds(-Global.numericUpDown_Term).ToString("HHmmss");
//             if (Global.dicHistoryPrice.ContainsKey(preDate) == false)
//             {
//                 MessageBox.Show($"현재가 데이터 수집중입니다...");
//                 return false;
//             }
                

            return true;
        }

        void SetTraceAccInfo()
        {
            Thread worker = new Thread(() =>
            {
                while(Global.stop == false)
                {
                    //lock (this.lockObjectPrice)
                    {
                        if (Global.dicHistoryPrice.Count > 0)
                        {
                            var diff = Global.dicHistoryPrice.Last().Value - Global.curStockPrice;
                            if (Math.Abs(diff) >= Global.oneDigit)
                                ReqMgr.Request_FutureAccountInfo();
                        }
                    }
                                      
                    Thread.Sleep(3000);
                }
            });
            worker.Start();
        }

        void SetMainTimer()
        {
            Thread timer = new Thread(() =>
            {
                var startTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, dateTimePicker_Start.Value.Hour, dateTimePicker_Start.Value.Minute, 00);
                var endTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, dateTimePicker_End.Value.Hour, dateTimePicker_End.Value.Minute, 00);
                while((startTime - DateTime.Now).TotalSeconds > 0)
                {
                    if (Global.stop)
                        break;

                    //this.UpdateRamainTime($"대기 시간 : {(int)(startTime - DateTime.Now).TotalSeconds} 초");
                    Thread.Sleep(1000);
                }

                while ((endTime - DateTime.Now).TotalSeconds > 0)
                {
                    if (Global.stop)
                        break;
                                 
                    //this.UpdateRamainTime($"남은 시간 : {(int)(endTime - DateTime.Now).TotalSeconds} 초");
                    Thread.Sleep(1000);
                }

                this.button_Stop_Click(null, null);

            });
            timer.Start();
        }

        void LoseCleanChecker(double purchaseValue_,bool purchaseBuy_, bool purchaseSell_, Action done_ , Action terminate_)
        {
            bool checkClean = false;

            bool loseCheckFlag = true;
            bool cleanCheckFlag = true;

            Thread loseChecker = new Thread(() =>
            {
                double diff = 0.0;
                while (Global.stop == false && loseCheckFlag)
                {
                    diff = Global.curStockPrice - purchaseValue_;
                    diff = Math.Round(diff, 3);
                    UpdateLoseDiff(diff.ToString());
                    double targetValue = Global.numericUpDown_LoseTick * Global.oneDigit;
                    targetValue = Math.Round(targetValue, 3);
                    if (purchaseBuy_)
                    {
                        if (diff <= -(targetValue))
                            break;
                    }
                    else if (purchaseSell_)
                    {
                        if (diff >= targetValue)
                            break;
                    }
                }

                if (loseCheckFlag)
                {
                    cleanCheckFlag = false;
                    var direction = (purchaseBuy_) ? "매수" : "매도";
                    Print_Log($"{direction} 손절 요청 - 현재가 : {Global.curStockPrice} / 구매가 : {purchaseValue_} /차이 : {diff}");
                }
                Print_Log($"손절 체크 종료");
            });
            loseChecker.Start();
        
            Thread cleanChecker = new Thread(() =>
            {
                while (Global.stop == false && cleanCheckFlag)
                {
                    double preValueForClean = Global.curStockPrice;
                    int cntForClean = Global.numericUpDown_CheckWinTerm;
                    Print_Log($"청산 데이터 수집");
                    while (Global.stop == false && cleanCheckFlag)
                    {
                        if (cntForClean < 0)
                            break;
                        cntForClean--;
                        Thread.Sleep(1000);
                    }

                    if (Global.stop)
                    {
                        //proc.DeleteAllProc();
                        terminate_();
                        break;
                    }

                    if (cleanCheckFlag == false)
                        break;

                    double diff = Global.curStockPrice - preValueForClean;
                    diff = Math.Round(diff, 3);
                    UpdateCleanDiff(diff.ToString());

                    Print_Log($"청산 데이터 체크 - 현재가 : {Global.curStockPrice} / 이전가 : {preValueForClean} / 차이 : {diff}");
                    double targetValue = Global.numericUpDown_WinTick * Global.oneDigit;
                    targetValue = Math.Round(targetValue, 3);
                    if (purchaseBuy_)
                    {
                        if (diff <= -(targetValue))
                        {
                            Print_Log($"매수 청산 요청 - 현재가 : {Global.curStockPrice} / 차이 : {diff}");
                            checkClean = true;
                            break;
                        }

                    }
                    else if (purchaseSell_)
                    {
                        if (diff >= targetValue)
                        {
                            Print_Log($"매도 청산 요청 - 현재가 : {Global.curStockPrice} / 차이 : {diff}");
                            checkClean = true;
                            break;
                        }
                    }
                }
                loseCheckFlag = false;
                Print_Log($"청산 체크 종료");
            });
            cleanChecker.Start();

            while (Global.stop == false)
            {
                if (loseChecker.IsAlive == false && cleanChecker.IsAlive == false)
                {
                    break;
                }

                Thread.Sleep(100);
            }

            ClearOrderAndStock(() =>
            {
                if (checkClean)
                    done_();
                else
                    terminate_();
            });
        }

        void TodayWinMoneyChecker(Action done_, Action terminate_)
        {
            Print_Log("당일매도손익 조회 시도");
            Thread.Sleep(2000);
            Print_Log($"당일매도손익 :{Global.curTodayWinmoney}");

            if (Global.curTodayWinmoney >= Global.numericUpDown_TodayWinMoney)
            {
                Print_Log($"현대 당일매도손익 목표({Global.numericUpDown_TodayWinMoney})달성 : {Global.curTodayWinmoney}");
                terminate_();
            }
            else
            {
                done_();
            }
        }

        void SetMainWorker()
        {
            Thread mainWorker = new Thread(() =>
            {
                var startTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, dateTimePicker_Start.Value.Hour, dateTimePicker_Start.Value.Minute, 00);
                //시작시간 대기...
                while((startTime - DateTime.Now).TotalSeconds > 0 && Global.stop == false)
                {
                    Thread.Sleep(1000);
                }

                ProcScript proc = new ProcScript();

                while (true)
                {

                    try
                    {
                        if (Global.stop)
                        {
                            proc.DeleteAllProc();
                            break;
                        }

                        if (proc.IsEmpty())
                        {
                            double preValue = Global.curStockPrice;
                            double curValue = Global.curStockPrice;
                            double purchaseValue = Global.curStockPrice;

                            UpdatePurchaseDiff("0");
                            UpdateCleanDiff("0");
                            UpdateLoseDiff("0");

                            proc.AddProc($"데이터 수집", (param_) => {
                                Print_Log($"[{proc.curProc.name}]");

                                Thread worker = new Thread(()=> {
                                    var remainSec = Global.numericUpDown_Term;

                                    while (Global.stop == false)
                                    {
                                        if (remainSec < 0)
                                            break;
                                        remainSec--;
                                        Thread.Sleep(1000);
                                    }

                                    if (Global.stop)
                                    {
                                        proc.DeleteAllProc();
                                    }
                                    else
                                    {
                                        curValue = Global.curStockPrice;
                                        Print_Log($"데이터 수집 완료 - 현재가 : {curValue} / 이전가 : {preValue}");
                                        proc.DoneProc();
                                    }
                                });
                                worker.Start();
                            }, null);

                            bool purchaseBuy = false;
                            bool purchaseSell = false;

                            proc.AddProc($"기준가 매매 체크", (param_) => {
                                Print_Log($"[{proc.curProc.name}]");

                                double diff = curValue - preValue;
                                diff = Math.Round(diff, 3);
                                UpdatePurchaseDiff(diff.ToString());
                                double targetValue = Global.numericUpDown_GoalTick * Global.oneDigit;
                                targetValue = Math.Round(targetValue,3);
                                Func<bool> CheckBuyRight = () => {
                                    if (Global.checkBox_BuyRight == false)
                                        return false;
                                    if (diff < targetValue)
                                        return false;

                                    Print_Log($"시장가 매수 매매조건 충족 - 차이 : {diff}");
                                    return true;
                                };

                                Func<bool> CheckSellRight = () =>
                                {
                                    if (Global.checkBox_SellRight == false)
                                        return false;
                                    if (diff > -(targetValue))
                                        return false;

                                    Print_Log($"시장가 매도 매매조건 충족 - 차이 : {diff}");
                                    return true;
                                };

                                purchaseBuy = CheckBuyRight();
                                purchaseSell = CheckSellRight();

                                Thread.Sleep(1000);

                                if (purchaseBuy || purchaseSell)
                                {
                                    proc.DoneProc();
                                }
                                else
                                {
                                    Print_Log($"시장가 매수 매도 충족 조건이 없음");
                                    proc.DeleteAllProc();
                                }
                                
                            }, null);

                            proc.AddProc("매매 진입", (param_) => {
                                Print_Log($"[{proc.curProc.name}]");

                                if (purchaseBuy)
                                {
                                    PurchaseBuyRight(Global.numericUpDown_PerCnt, (remainSec_) =>
                                    {
                                        if (remainSec_ <= 0)
                                        {
                                            Print_Log($"시장가 매수 실패");
                                            CancelOrder();
                                            proc.DeleteAllProc();
                                        }
                                        else
                                        {
                                            purchaseValue = Global.curStockPrice;
                                            Print_Log($"시장가 매수 {Global.curStockPrice}");
                                            proc.DoneProc();
                                        }

                                    }, Global.numericUpDown_LimitOrderingTime);
                                }

                                if (purchaseSell)
                                {
                                    PurchaseSellRight(Global.numericUpDown_PerCnt, (remainSec_) =>
                                    {
                                        if (remainSec_ <= 0)
                                        {
                                            Print_Log($"시장가 매도 실패");
                                            CancelOrder();
                                            proc.DeleteAllProc();
                                        }
                                        else
                                        {
                                            purchaseValue = Global.curStockPrice;
                                            Print_Log($"시장가 매도 {Global.curStockPrice}");
                                            proc.DoneProc();
                                        }

                                    }, Global.numericUpDown_LimitOrderingTime);
                                }
 
                            }, null);

                            proc.AddProc("손절 , 청산 체크", (param_) => {
                                Print_Log($"[{proc.curProc.name}]");

                                LoseCleanChecker(purchaseValue, purchaseBuy, purchaseSell, () =>
                                {
                                    proc.DoneProc();
                                }, ()=> {
                                    proc.DeleteAllProc();
                                });
                            }, null);

                            if (Global.checkBox_TodayWinMoney)
                            {
                                proc.AddProc("당일매도손익 체크", (param_) => {
                                    Print_Log($"[{proc.curProc.name}]");
                                    TodayWinMoneyChecker(() => {
                                        proc.DoneProc();
                                    }, () => {
                                        proc.DeleteAllProc();
                                    });
                                },null);
                            }

                            if (Global.checkBox_ClearOption)
                            {
                                //50번 정도 체크하면...충분하겠지
                                for(int i=0; i<50; ++i)
                                {
                                    proc.AddProc($"청산옵션", (param_) =>
                                    {
                                        Print_Log($"[{proc.curProc.name}]");

                                        if (purchaseBuy && Global.checkBox_SellRight)
                                        {
                                            PurchaseSellRight(Global.numericUpDown_PerCnt, (remainSec_) =>
                                            {
                                                if (remainSec_ <= 0)
                                                {
                                                    Print_Log($"시장가 청산 반대 매도 주문 실패!...");
                                                    CancelOrder();
                                                    proc.DeleteAllProc();
                                                }
                                                else
                                                {
                                                    purchaseBuy = false;
                                                    purchaseSell = true;
                                                    purchaseValue = Global.curStockPrice;
                                                    Print_Log($"시장가 청산 반대 매도 주문 체결가 {Global.curStockPrice}");
                                                    proc.DoneProc();
                                                }

                                            }, Global.numericUpDown_LimitOrderingTime);
                                        }
                                        else if (purchaseSell && Global.checkBox_BuyRight)
                                        {
                                            PurchaseBuyRight(Global.numericUpDown_PerCnt, (remainSec_) =>
                                            {
                                                if (remainSec_ <= 0)
                                                {
                                                    Print_Log($"시장가 청산 반대 매수 주문 실패!...");
                                                    CancelOrder();
                                                    proc.DeleteAllProc();
                                                }
                                                else
                                                {
                                                    purchaseBuy = true;
                                                    purchaseSell = false;
                                                    purchaseValue = Global.curStockPrice;
                                                    Print_Log($"시장가 청산 반대 매수 주문 체결가 {Global.curStockPrice}");
                                                    proc.DoneProc();
                                                }
                                            }, Global.numericUpDown_LimitOrderingTime);
                                        }
                                        else
                                        {
                                            Print_Log($"조건에 맞는 매매가 없습니다.");
                                            proc.DeleteAllProc();
                                        }
                                    }, null);

                                    proc.AddProc($"청산옵션 손절,청산 체크", (param_) =>
                                    {
                                        Print_Log($"[{proc.curProc.name}]");

                                        LoseCleanChecker(purchaseValue, purchaseBuy, purchaseSell, () =>
                                        {
                                            proc.DoneProc();
                                        }, () =>
                                        {
                                            proc.DeleteAllProc();
                                        });

                                    }, null);

                                    if (Global.checkBox_TodayWinMoney)
                                    {
                                        proc.AddProc("당일매도손익 체크", (param_) =>
                                        {
                                            Print_Log($"[{proc.curProc.name}]");
                                            TodayWinMoneyChecker(() =>
                                            {
                                                proc.DoneProc();
                                            }, () =>
                                            {
                                                proc.DeleteAllProc();
                                            });
                                        }, null);
                                    }
                                }

                            }                                                  
                        }
                    }
                    catch(Exception er)
                    {
#if DEBUG
                        Print_Log($"er : {er.Message}");
#endif
                    }
  

                    Thread.Sleep(1000);
                }


                var endTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, dateTimePicker_End.Value.Hour, dateTimePicker_End.Value.Minute, 00);
                if ((endTime - DateTime.Now).TotalSeconds <= 0)
                {
                    Print_Log($"종료시간 자동청산");
                    bool cleanOrder = false;
                    ClearOrderAndStock(() => {
                        cleanOrder = true;
                    });

                    while(cleanOrder == false)
                    {
                        Thread.Sleep(500);
                    }

                    Print_Log("작업 끝");
                }
                else
                {
                    Print_Log("정지 완료");
                }
               
                this.Invoke(new MethodInvoker(delegate ()
                {
                    button_Start.Enabled = true;
                    button_Stop.Enabled = false;
                    tableLayoutPanel_Option.Enabled = true;

                    button_BuyRight.Enabled = true;
                    button_SellRight.Enabled = true;
                    button_AllClear.Enabled = true;
                }));

                Global.curStockPrice = 0.0f;
                Global.purchasePrice = 0.0f;
            });
            mainWorker.Start();
        }

        private void button_Start_Click(object sender, EventArgs e)
        {
            if (this.IsValidStateToStart())
            {
                Global.UpdateValueFrom(this);

                Global.stop = false;
                Print_Log("시작");

                //this.SetTraceAccInfo();

                this.SetMainTimer();

                this.SetMainWorker();

                this.Invoke(new MethodInvoker(delegate ()
                {
                    button_Start.Enabled = false;
                    button_Stop.Enabled = true;
                    tableLayoutPanel_Option.Enabled = false;

                    button_BuyRight.Enabled = false;
                    button_SellRight.Enabled = false;
                    button_AllClear.Enabled = false;
                }));         
            }
            else
            {

            }
        }

        private void button_Stop_Click(object sender, EventArgs e)
        {
            Global.stop = true;

            Print_Log("정지");

            button_Stop.Invoke(new MethodInvoker(delegate ()
            {
                button_Stop.Enabled = false;
            }));  
        }

        void PurchaseBuyRight(int cnt_,Action<int> callback_ , int limitSec_ = 100000)
        {
            //lock (this.lockObject)
            {
                ReqMgr.Request_NewOrder_Buy("선물주문", cnt_);

                int goalCnt = cnt_;
                string purchaseType = "2";
                var stock = Global.listStockInfo.Find(x => x.종목코드 == Global.comboBox_KospiCode);
                if (stock != null)
                {
                    int cntHave = int.Parse(stock.보유수량);
                    if (stock.매매구분 == "2")
                    {
                        goalCnt += cntHave;
                    }
                    else
                    {
                        if (cnt_ >= cntHave)
                        {
                            goalCnt -= cntHave;
                        }
                        else
                        {
                            purchaseType = "1";
                            goalCnt = cntHave - cnt_;
                        }
                    }
                }

                var thread = new Thread(() =>
                {
                    int limitSec = limitSec_;
                    while (true)
                    {
                        if (limitSec < 0)
                            break;

                        var checkStock = Global.listStockInfo.Find(x => x.종목코드 == Global.comboBox_KospiCode && x.매매구분 == purchaseType);
                        if (goalCnt == 0)
                        {
                            checkStock = Global.listStockInfo.Find(x => x.종목코드 == Global.comboBox_KospiCode);
                            if (checkStock == null)
                                break;
                        }
                        else
                        {
                            if (checkStock != null)
                            {
                                var have = int.Parse(checkStock.보유수량);
                                if (have == goalCnt)
                                    break;
                            }
                        }

                        Thread.Sleep(1000);
                        limitSec--;
                    }

                    callback_(limitSec);
                });
                thread.Start();
            }
            
        }

        void PurchaseSellRight(int cnt_,Action<int> callback_, int limitSec_ = 100000)
        {
            //lock (this.lockObject)
            {
                ReqMgr.Request_NewOrder_Sell("선물주문", cnt_);

                int goalCnt = cnt_;
                string purchaseType = "1";
                var stock = Global.listStockInfo.Find(x => x.종목코드 == Global.comboBox_KospiCode);
                if (stock != null)
                {
                    int cntHave = int.Parse(stock.보유수량);
                    if (stock.매매구분 == "1")
                    {
                        goalCnt += cntHave;
                    }
                    else
                    {
                        if (cnt_ >= cntHave)
                        {
                            goalCnt -= cntHave;
                        }
                        else
                        {
                            purchaseType = "2";
                            goalCnt = cntHave - cnt_;
                        }
                    }

                }

                var thread = new Thread(() =>
                {
                    int limitSec = limitSec_;
                    while (true)
                    {
                        if (limitSec < 0)
                            break;

                        var checkStock = Global.listStockInfo.Find(x => x.종목코드 == Global.comboBox_KospiCode && x.매매구분 == purchaseType);
                        if (goalCnt == 0)
                        {
                            checkStock = Global.listStockInfo.Find(x => x.종목코드 == Global.comboBox_KospiCode);
                            if (checkStock == null)
                                break;
                        }
                        else
                        {
                            if (checkStock != null)
                            {
                                var have = int.Parse(checkStock.보유수량);
                                if (have == goalCnt)
                                    break;
                            }
                        }

                        Thread.Sleep(1000);
                        limitSec--;
                    }

                    callback_(limitSec);
                });
                thread.Start();
            }
            
        }

        void CancelOrder()
        {
            foreach (var item in Global.listOrderIfo)
            {
                if (string.IsNullOrEmpty(item.종목코드))
                    continue;

                if (string.IsNullOrEmpty(item.주문수량))
                    continue;

                if (int.Parse(item.주문수량.Trim()) <= 0)
                    continue;

                var retCancel = ReqMgr.Request_CancelOrder("주문취소", int.Parse(item.주문수량.Trim()), item.주문번호);
            }

            Global.listOrderIfo.Clear();
        }

        void ClearOrderAndStock(Action callback_)
        {
            //lock (this.lockObject)
            {
                foreach (var item in Global.listOrderIfo)
                {
                    if (string.IsNullOrEmpty(item.종목코드))
                        continue;

                    if (string.IsNullOrEmpty(item.주문수량))
                        continue;

                    if (int.Parse(item.주문수량.Trim()) <= 0)
                        continue;

                    var retCancel = ReqMgr.Request_CancelOrder("주문취소", int.Parse(item.주문수량.Trim()), item.주문번호);
                }

                foreach (StockInfo item in Global.listStockInfo)
                {
                    if (string.IsNullOrEmpty(item.종목코드))
                        continue;

                    if (string.IsNullOrEmpty(item.보유수량))
                        continue;

                    if (int.Parse(item.보유수량.Trim()) <= 0)
                        continue;

                    if (item.매매구분.Contains("2"))
                        ReqMgr.Request_NewOrder_Sell("보유주식청산", int.Parse(item.보유수량.Trim()));
                    else
                        ReqMgr.Request_NewOrder_Buy("보유주식청산", int.Parse(item.보유수량.Trim()));
                }


                var thread = new Thread(() =>
                {
                    while (
                    Global.listStockInfo.FindAll(x => x.종목코드 == Global.comboBox_KospiCode).Count > 0
                    || Global.listOrderIfo.FindAll(x => x.종목코드 == Global.comboBox_KospiCode).Count > 0
                    )
                    {
                        Thread.Sleep(1000);
                    }

                    callback_();
                });

                thread.Start();
            }
            
        }

        private void button_AllClear_Click(object sender, EventArgs e)
        {
            if (KiwoomAPI.Get.GetConnectState() == 0)
                return;

            

            button_AllClear.Enabled = false;

            this.procScript.AddProc($"청산버튼", (param_) => {
                Print_Log("청산! 시작");
                this.ClearOrderAndStock(() =>
                {
                    procScript.DoneProc();
                    Print_Log("청산! 완료");
                    button_AllClear.Invoke(new MethodInvoker(delegate () { button_AllClear.Enabled = true; }));
                });
            }, null);          
        }

        private void button_BuyRight_Click(object sender, EventArgs e)
        {
            if (KiwoomAPI.Get.GetConnectState() == 0)
                return;
           
            button_BuyRight.Enabled = false;
            numericUpDown_BuyRightCnt.Enabled = false;

            this.procScript.AddProc($"매수버튼 {Global.numericUpDown_BuyRightCnt}", (param_) =>
            {
                Print_Log($"현재가({Global.curStockPrice}) 시장가 매수주문");
                PurchaseBuyRight(Global.numericUpDown_BuyRightCnt, (remainSec_) =>
                {
                    this.procScript.DoneProc();
                    if (remainSec_ >= 0)
                    {
                        Print_Log($"시간 시장가 매수 체결가격 {Global.curStockPrice}");
                    }
                    else
                    {
                        Print_Log($"시간초과로 주문취소");
                        CancelOrder();
                    }

                    this.Invoke(new MethodInvoker(delegate ()
                    {
                        button_BuyRight.Enabled = true;
                        numericUpDown_BuyRightCnt.Enabled = true;
                    }));
                },Global.numericUpDown_LimitOrderingTime);
            }, null);

            
        }

        private void button_SellRight_Click(object sender, EventArgs e)
        {
            if (KiwoomAPI.Get.GetConnectState() == 0)
                return;
          
            button_SellRight.Enabled = false;
            numericUpDown_SellRightCnt.Enabled = false;

            this.procScript.AddProc($"매도버튼 {Global.numericUpDown_SellRightCnt}", (param_) => {
                Print_Log($"현재가({Global.curStockPrice}) 시장가 매도주문");
                PurchaseSellRight(Global.numericUpDown_SellRightCnt, (remainSec_) =>
                {
                    this.procScript.DoneProc();
                    if (remainSec_ >= 0)
                    {
                        Print_Log($"시간 시장가 매도 체결가격 {Global.curStockPrice}");
                    }
                    else
                    {
                        Print_Log($"시간초과로 주문취소");
                        CancelOrder();
                    }

                    this.Invoke(new MethodInvoker(delegate ()
                    {
                        button_SellRight.Enabled = true;
                        numericUpDown_SellRightCnt.Enabled = true;
                    }));
                },Global.numericUpDown_LimitOrderingTime);
            } , null);           
        }

        private void button_ClearLog_Click(object sender, EventArgs e)
        {
            listBox_Log.Items.Clear();
        }

        private void button_SaveLog_Click(object sender, EventArgs e)
        {
            Util.SaveTxtFile($"Log_{DateTime.Now.ToString("yyyyMMddHHmmss")}.txt" , listBox_Log.Items.OfType<string>().ToList());
        }

        private void button_Test_Click(object sender, EventArgs e)
        {
            Global.stop = false;
            this.SetMainTimer();
        }

        Point prevPosition = new Point(0,0);
        ToolTip tooltip = new ToolTip();

        private void button_UpdateWin_Click(object sender, EventArgs e)
        {
            if (KiwoomAPI.Get.GetConnectState() == 0)
                return;

            ReqMgr.Request_FutureAccountInfo();
            ReqMgr.Request_FutureAccountInfo2();
        }
    }
}
