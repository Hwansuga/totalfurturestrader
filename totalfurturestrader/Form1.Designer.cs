﻿namespace totalfurturestrader
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label_Server = new System.Windows.Forms.Label();
            this.label_UserID = new System.Windows.Forms.Label();
            this.label_UserName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loginToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label_KospiPrice = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox_KospiCode = new System.Windows.Forms.ComboBox();
            this.button_TraceStock = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox_Account = new System.Windows.Forms.ComboBox();
            this.button_CheckAcc = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel_Option = new System.Windows.Forms.TableLayoutPanel();
            this.checkBox_TodayWinMoney = new System.Windows.Forms.CheckBox();
            this.numericUpDown_TodayWinMoney = new System.Windows.Forms.NumericUpDown();
            this.label_LoseDiff = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label_ClearDiff = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label_PurchaseDiff = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.checkBox_ClearOption = new System.Windows.Forms.CheckBox();
            this.numericUpDown_Term = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.numericUpDown_PerCnt = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_GoalTick = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.numericUpDown_LimitOrderingTime = new System.Windows.Forms.NumericUpDown();
            this.checkBox_BuyRight = new System.Windows.Forms.CheckBox();
            this.checkBox_SellRight = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.dateTimePicker_End = new System.Windows.Forms.DateTimePicker();
            this.label18 = new System.Windows.Forms.Label();
            this.dateTimePicker_Start = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDown_LoseTick = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.numericUpDown_WinTick = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.numericUpDown_CheckWinTerm = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.numericUpDown_SellRightCnt = new System.Windows.Forms.NumericUpDown();
            this.button_AllClear = new System.Windows.Forms.Button();
            this.button_Stop = new System.Windows.Forms.Button();
            this.button_Start = new System.Windows.Forms.Button();
            this.button_BuyRight = new System.Windows.Forms.Button();
            this.button_SellRight = new System.Windows.Forms.Button();
            this.numericUpDown_BuyRightCnt = new System.Windows.Forms.NumericUpDown();
            this.panel2 = new System.Windows.Forms.Panel();
            this.listBox_Price = new System.Windows.Forms.ListBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button_Test = new System.Windows.Forms.Button();
            this.button_SaveLog = new System.Windows.Forms.Button();
            this.button_ClearLog = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.listBox_Log = new System.Windows.Forms.ListBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button_UpdateWin = new System.Windows.Forms.Button();
            this.label_WinMoney = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label_TodayWinMoney = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label_HaveMoney = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.button_RemoveLog = new System.Windows.Forms.Button();
            this.label_StandardPrice = new System.Windows.Forms.Label();
            this.label_RemainTime = new System.Windows.Forms.Label();
            this.dataGridView_OderInfo = new System.Windows.Forms.DataGridView();
            this.label10 = new System.Windows.Forms.Label();
            this.dataGridView_HavingStockInfo = new System.Windows.Forms.DataGridView();
            this.label9 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.chart_Price = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.axKHOpenAPI1 = new AxKHOpenAPILib.AxKHOpenAPI();
            this.tableLayoutPanel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel_Option.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_TodayWinMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Term)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_PerCnt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_GoalTick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_LimitOrderingTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_LoseTick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_WinTick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_CheckWinTerm)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SellRightCnt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BuyRightCnt)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_OderInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_HavingStockInfo)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart_Price)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axKHOpenAPI1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label_Server, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label_UserID, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label_UserName, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 137);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(232, 86);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // label_Server
            // 
            this.label_Server.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_Server.AutoSize = true;
            this.label_Server.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label_Server.Location = new System.Drawing.Point(119, 56);
            this.label_Server.Name = "label_Server";
            this.label_Server.Size = new System.Drawing.Size(110, 30);
            this.label_Server.TabIndex = 9;
            this.label_Server.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_UserID
            // 
            this.label_UserID.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_UserID.AutoSize = true;
            this.label_UserID.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label_UserID.Location = new System.Drawing.Point(119, 28);
            this.label_UserID.Name = "label_UserID";
            this.label_UserID.Size = new System.Drawing.Size(110, 28);
            this.label_UserID.TabIndex = 8;
            this.label_UserID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_UserName
            // 
            this.label_UserName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_UserName.AutoSize = true;
            this.label_UserName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label_UserName.Location = new System.Drawing.Point(119, 0);
            this.label_UserName.Name = "label_UserName";
            this.label_UserName.Size = new System.Drawing.Size(110, 28);
            this.label_UserName.TabIndex = 7;
            this.label_UserName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.Silver;
            this.label3.Location = new System.Drawing.Point(3, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 28);
            this.label3.TabIndex = 2;
            this.label3.Text = "아이디";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.Silver;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 28);
            this.label2.TabIndex = 1;
            this.label2.Text = "사용자이름";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.Color.Silver;
            this.label5.Location = new System.Drawing.Point(3, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 30);
            this.label5.TabIndex = 4;
            this.label5.Text = "접속서버";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(797, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loginToolStripMenuItem,
            this.logOutToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // loginToolStripMenuItem
            // 
            this.loginToolStripMenuItem.Name = "loginToolStripMenuItem";
            this.loginToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.loginToolStripMenuItem.Text = "Login";
            this.loginToolStripMenuItem.Click += new System.EventHandler(this.loginToolStripMenuItem_Click);
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.logOutToolStripMenuItem.Text = "LogOut";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label_KospiPrice, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.comboBox_KospiCode, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.button_TraceStock, 1, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 57);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(232, 80);
            this.tableLayoutPanel2.TabIndex = 7;
            // 
            // label_KospiPrice
            // 
            this.label_KospiPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_KospiPrice.AutoSize = true;
            this.label_KospiPrice.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label_KospiPrice.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_KospiPrice.Location = new System.Drawing.Point(119, 26);
            this.label_KospiPrice.Name = "label_KospiPrice";
            this.label_KospiPrice.Size = new System.Drawing.Size(110, 26);
            this.label_KospiPrice.TabIndex = 14;
            this.label_KospiPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.Color.Silver;
            this.label7.Location = new System.Drawing.Point(3, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 26);
            this.label7.TabIndex = 13;
            this.label7.Text = "현재가";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.Color.Silver;
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 26);
            this.label6.TabIndex = 11;
            this.label6.Text = "KOSPI200 종목";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBox_KospiCode
            // 
            this.comboBox_KospiCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_KospiCode.BackColor = System.Drawing.SystemColors.Window;
            this.comboBox_KospiCode.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox_KospiCode.FormattingEnabled = true;
            this.comboBox_KospiCode.Location = new System.Drawing.Point(119, 3);
            this.comboBox_KospiCode.Name = "comboBox_KospiCode";
            this.comboBox_KospiCode.Size = new System.Drawing.Size(110, 20);
            this.comboBox_KospiCode.TabIndex = 12;
            // 
            // button_TraceStock
            // 
            this.button_TraceStock.BackColor = System.Drawing.Color.Transparent;
            this.button_TraceStock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_TraceStock.ForeColor = System.Drawing.Color.Silver;
            this.button_TraceStock.Location = new System.Drawing.Point(119, 55);
            this.button_TraceStock.Name = "button_TraceStock";
            this.button_TraceStock.Size = new System.Drawing.Size(110, 21);
            this.button_TraceStock.TabIndex = 15;
            this.button_TraceStock.Text = "종목 조회";
            this.button_TraceStock.UseVisualStyleBackColor = false;
            this.button_TraceStock.Click += new System.EventHandler(this.button_TraceStock_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.comboBox_Account, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(232, 57);
            this.tableLayoutPanel4.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.Silver;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 28);
            this.label4.TabIndex = 3;
            this.label4.Text = "계좌번호";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBox_Account
            // 
            this.comboBox_Account.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_Account.BackColor = System.Drawing.SystemColors.Window;
            this.comboBox_Account.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox_Account.FormattingEnabled = true;
            this.comboBox_Account.Location = new System.Drawing.Point(119, 3);
            this.comboBox_Account.Name = "comboBox_Account";
            this.comboBox_Account.Size = new System.Drawing.Size(110, 20);
            this.comboBox_Account.TabIndex = 5;
            this.comboBox_Account.SelectedIndexChanged += new System.EventHandler(this.comboBox_Account_SelectedIndexChanged);
            // 
            // button_CheckAcc
            // 
            this.button_CheckAcc.BackColor = System.Drawing.Color.Transparent;
            this.button_CheckAcc.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_CheckAcc.Enabled = false;
            this.button_CheckAcc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_CheckAcc.ForeColor = System.Drawing.Color.Silver;
            this.button_CheckAcc.Location = new System.Drawing.Point(0, 223);
            this.button_CheckAcc.Name = "button_CheckAcc";
            this.button_CheckAcc.Size = new System.Drawing.Size(232, 25);
            this.button_CheckAcc.TabIndex = 14;
            this.button_CheckAcc.Text = "계좌조회";
            this.button_CheckAcc.UseVisualStyleBackColor = false;
            this.button_CheckAcc.Click += new System.EventHandler(this.button_CheckAcc_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.panel1.Controls.Add(this.tableLayoutPanel_Option);
            this.panel1.Controls.Add(this.tableLayoutPanel3);
            this.panel1.Controls.Add(this.button_CheckAcc);
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Controls.Add(this.tableLayoutPanel4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(232, 850);
            this.panel1.TabIndex = 20;
            // 
            // tableLayoutPanel_Option
            // 
            this.tableLayoutPanel_Option.ColumnCount = 2;
            this.tableLayoutPanel_Option.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56.03448F));
            this.tableLayoutPanel_Option.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.96552F));
            this.tableLayoutPanel_Option.Controls.Add(this.checkBox_TodayWinMoney, 0, 10);
            this.tableLayoutPanel_Option.Controls.Add(this.numericUpDown_TodayWinMoney, 1, 10);
            this.tableLayoutPanel_Option.Controls.Add(this.label_LoseDiff, 1, 12);
            this.tableLayoutPanel_Option.Controls.Add(this.label23, 0, 12);
            this.tableLayoutPanel_Option.Controls.Add(this.label_ClearDiff, 1, 9);
            this.tableLayoutPanel_Option.Controls.Add(this.label22, 0, 9);
            this.tableLayoutPanel_Option.Controls.Add(this.label_PurchaseDiff, 1, 5);
            this.tableLayoutPanel_Option.Controls.Add(this.label21, 0, 5);
            this.tableLayoutPanel_Option.Controls.Add(this.checkBox_ClearOption, 0, 8);
            this.tableLayoutPanel_Option.Controls.Add(this.numericUpDown_Term, 1, 1);
            this.tableLayoutPanel_Option.Controls.Add(this.label11, 0, 0);
            this.tableLayoutPanel_Option.Controls.Add(this.label12, 0, 1);
            this.tableLayoutPanel_Option.Controls.Add(this.label13, 0, 2);
            this.tableLayoutPanel_Option.Controls.Add(this.numericUpDown_PerCnt, 1, 0);
            this.tableLayoutPanel_Option.Controls.Add(this.numericUpDown_GoalTick, 1, 2);
            this.tableLayoutPanel_Option.Controls.Add(this.label15, 0, 3);
            this.tableLayoutPanel_Option.Controls.Add(this.numericUpDown_LimitOrderingTime, 1, 3);
            this.tableLayoutPanel_Option.Controls.Add(this.checkBox_BuyRight, 0, 4);
            this.tableLayoutPanel_Option.Controls.Add(this.checkBox_SellRight, 1, 4);
            this.tableLayoutPanel_Option.Controls.Add(this.label19, 0, 14);
            this.tableLayoutPanel_Option.Controls.Add(this.dateTimePicker_End, 1, 14);
            this.tableLayoutPanel_Option.Controls.Add(this.label18, 0, 13);
            this.tableLayoutPanel_Option.Controls.Add(this.dateTimePicker_Start, 1, 13);
            this.tableLayoutPanel_Option.Controls.Add(this.label1, 0, 11);
            this.tableLayoutPanel_Option.Controls.Add(this.numericUpDown_LoseTick, 1, 11);
            this.tableLayoutPanel_Option.Controls.Add(this.label8, 0, 7);
            this.tableLayoutPanel_Option.Controls.Add(this.numericUpDown_WinTick, 1, 7);
            this.tableLayoutPanel_Option.Controls.Add(this.label14, 0, 6);
            this.tableLayoutPanel_Option.Controls.Add(this.numericUpDown_CheckWinTerm, 1, 6);
            this.tableLayoutPanel_Option.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel_Option.Location = new System.Drawing.Point(0, 497);
            this.tableLayoutPanel_Option.Name = "tableLayoutPanel_Option";
            this.tableLayoutPanel_Option.RowCount = 15;
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel_Option.Size = new System.Drawing.Size(232, 353);
            this.tableLayoutPanel_Option.TabIndex = 16;
            // 
            // checkBox_TodayWinMoney
            // 
            this.checkBox_TodayWinMoney.AutoSize = true;
            this.checkBox_TodayWinMoney.ForeColor = System.Drawing.Color.Silver;
            this.checkBox_TodayWinMoney.Location = new System.Drawing.Point(3, 233);
            this.checkBox_TodayWinMoney.Name = "checkBox_TodayWinMoney";
            this.checkBox_TodayWinMoney.Size = new System.Drawing.Size(96, 16);
            this.checkBox_TodayWinMoney.TabIndex = 33;
            this.checkBox_TodayWinMoney.Text = "당일매도손익";
            this.checkBox_TodayWinMoney.UseVisualStyleBackColor = true;
            this.checkBox_TodayWinMoney.Visible = false;
            // 
            // numericUpDown_TodayWinMoney
            // 
            this.numericUpDown_TodayWinMoney.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_TodayWinMoney.Location = new System.Drawing.Point(132, 233);
            this.numericUpDown_TodayWinMoney.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numericUpDown_TodayWinMoney.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_TodayWinMoney.Name = "numericUpDown_TodayWinMoney";
            this.numericUpDown_TodayWinMoney.Size = new System.Drawing.Size(97, 21);
            this.numericUpDown_TodayWinMoney.TabIndex = 24;
            this.numericUpDown_TodayWinMoney.Value = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numericUpDown_TodayWinMoney.Visible = false;
            // 
            // label_LoseDiff
            // 
            this.label_LoseDiff.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_LoseDiff.AutoSize = true;
            this.label_LoseDiff.BackColor = System.Drawing.Color.Transparent;
            this.label_LoseDiff.ForeColor = System.Drawing.Color.Silver;
            this.label_LoseDiff.Location = new System.Drawing.Point(132, 276);
            this.label_LoseDiff.Name = "label_LoseDiff";
            this.label_LoseDiff.Size = new System.Drawing.Size(97, 23);
            this.label_LoseDiff.TabIndex = 19;
            this.label_LoseDiff.Text = ".....";
            this.label_LoseDiff.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.ForeColor = System.Drawing.Color.Silver;
            this.label23.Location = new System.Drawing.Point(3, 276);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(123, 23);
            this.label23.TabIndex = 26;
            this.label23.Text = "손절감지";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_ClearDiff
            // 
            this.label_ClearDiff.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_ClearDiff.AutoSize = true;
            this.label_ClearDiff.BackColor = System.Drawing.Color.Transparent;
            this.label_ClearDiff.ForeColor = System.Drawing.Color.Silver;
            this.label_ClearDiff.Location = new System.Drawing.Point(132, 207);
            this.label_ClearDiff.Name = "label_ClearDiff";
            this.label_ClearDiff.Size = new System.Drawing.Size(97, 23);
            this.label_ClearDiff.TabIndex = 18;
            this.label_ClearDiff.Text = ".....";
            this.label_ClearDiff.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.ForeColor = System.Drawing.Color.Silver;
            this.label22.Location = new System.Drawing.Point(3, 207);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(123, 23);
            this.label22.TabIndex = 18;
            this.label22.Text = "청산감지";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_PurchaseDiff
            // 
            this.label_PurchaseDiff.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_PurchaseDiff.AutoSize = true;
            this.label_PurchaseDiff.BackColor = System.Drawing.Color.Transparent;
            this.label_PurchaseDiff.ForeColor = System.Drawing.Color.Silver;
            this.label_PurchaseDiff.Location = new System.Drawing.Point(132, 115);
            this.label_PurchaseDiff.Name = "label_PurchaseDiff";
            this.label_PurchaseDiff.Size = new System.Drawing.Size(97, 23);
            this.label_PurchaseDiff.TabIndex = 17;
            this.label_PurchaseDiff.Text = ".....";
            this.label_PurchaseDiff.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.ForeColor = System.Drawing.Color.Silver;
            this.label21.Location = new System.Drawing.Point(3, 115);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(123, 23);
            this.label21.TabIndex = 17;
            this.label21.Text = "진입감지";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkBox_ClearOption
            // 
            this.checkBox_ClearOption.AutoSize = true;
            this.checkBox_ClearOption.ForeColor = System.Drawing.Color.Silver;
            this.checkBox_ClearOption.Location = new System.Drawing.Point(3, 187);
            this.checkBox_ClearOption.Name = "checkBox_ClearOption";
            this.checkBox_ClearOption.Size = new System.Drawing.Size(72, 16);
            this.checkBox_ClearOption.TabIndex = 32;
            this.checkBox_ClearOption.Text = "청산옵션";
            this.checkBox_ClearOption.UseVisualStyleBackColor = true;
            // 
            // numericUpDown_Term
            // 
            this.numericUpDown_Term.Location = new System.Drawing.Point(132, 26);
            this.numericUpDown_Term.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_Term.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_Term.Name = "numericUpDown_Term";
            this.numericUpDown_Term.Size = new System.Drawing.Size(97, 21);
            this.numericUpDown_Term.TabIndex = 19;
            this.numericUpDown_Term.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.ForeColor = System.Drawing.Color.Silver;
            this.label11.Location = new System.Drawing.Point(3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(123, 23);
            this.label11.TabIndex = 4;
            this.label11.Text = "매매단위";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.ForeColor = System.Drawing.Color.Silver;
            this.label12.Location = new System.Drawing.Point(3, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(123, 23);
            this.label12.TabIndex = 5;
            this.label12.Text = "매매 시간 단위(초)";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.ForeColor = System.Drawing.Color.Silver;
            this.label13.Location = new System.Drawing.Point(3, 46);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(123, 23);
            this.label13.TabIndex = 6;
            this.label13.Text = "자동매매기준틱수";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDown_PerCnt
            // 
            this.numericUpDown_PerCnt.Location = new System.Drawing.Point(132, 3);
            this.numericUpDown_PerCnt.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown_PerCnt.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_PerCnt.Name = "numericUpDown_PerCnt";
            this.numericUpDown_PerCnt.Size = new System.Drawing.Size(97, 21);
            this.numericUpDown_PerCnt.TabIndex = 19;
            this.numericUpDown_PerCnt.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericUpDown_GoalTick
            // 
            this.numericUpDown_GoalTick.Location = new System.Drawing.Point(132, 49);
            this.numericUpDown_GoalTick.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_GoalTick.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_GoalTick.Name = "numericUpDown_GoalTick";
            this.numericUpDown_GoalTick.Size = new System.Drawing.Size(97, 21);
            this.numericUpDown_GoalTick.TabIndex = 22;
            this.numericUpDown_GoalTick.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.ForeColor = System.Drawing.Color.Silver;
            this.label15.Location = new System.Drawing.Point(3, 69);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(123, 23);
            this.label15.TabIndex = 8;
            this.label15.Text = "체결 제한시간(초)";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDown_LimitOrderingTime
            // 
            this.numericUpDown_LimitOrderingTime.Location = new System.Drawing.Point(132, 72);
            this.numericUpDown_LimitOrderingTime.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown_LimitOrderingTime.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown_LimitOrderingTime.Name = "numericUpDown_LimitOrderingTime";
            this.numericUpDown_LimitOrderingTime.Size = new System.Drawing.Size(97, 21);
            this.numericUpDown_LimitOrderingTime.TabIndex = 20;
            this.numericUpDown_LimitOrderingTime.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // checkBox_BuyRight
            // 
            this.checkBox_BuyRight.AutoSize = true;
            this.checkBox_BuyRight.Checked = true;
            this.checkBox_BuyRight.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_BuyRight.ForeColor = System.Drawing.Color.Silver;
            this.checkBox_BuyRight.Location = new System.Drawing.Point(3, 95);
            this.checkBox_BuyRight.Name = "checkBox_BuyRight";
            this.checkBox_BuyRight.Size = new System.Drawing.Size(76, 16);
            this.checkBox_BuyRight.TabIndex = 23;
            this.checkBox_BuyRight.Text = "매수 매매";
            this.checkBox_BuyRight.UseVisualStyleBackColor = true;
            // 
            // checkBox_SellRight
            // 
            this.checkBox_SellRight.AutoSize = true;
            this.checkBox_SellRight.Checked = true;
            this.checkBox_SellRight.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_SellRight.ForeColor = System.Drawing.Color.Silver;
            this.checkBox_SellRight.Location = new System.Drawing.Point(132, 95);
            this.checkBox_SellRight.Name = "checkBox_SellRight";
            this.checkBox_SellRight.Size = new System.Drawing.Size(76, 16);
            this.checkBox_SellRight.TabIndex = 24;
            this.checkBox_SellRight.Text = "매도 매매";
            this.checkBox_SellRight.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.ForeColor = System.Drawing.Color.Silver;
            this.label19.Location = new System.Drawing.Point(3, 322);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(123, 31);
            this.label19.TabIndex = 17;
            this.label19.Text = "종료시간";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dateTimePicker_End
            // 
            this.dateTimePicker_End.CustomFormat = "HH시 mm분";
            this.dateTimePicker_End.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_End.Location = new System.Drawing.Point(132, 325);
            this.dateTimePicker_End.Name = "dateTimePicker_End";
            this.dateTimePicker_End.ShowUpDown = true;
            this.dateTimePicker_End.Size = new System.Drawing.Size(97, 21);
            this.dateTimePicker_End.TabIndex = 18;
            this.dateTimePicker_End.Value = new System.DateTime(2020, 10, 26, 15, 19, 0, 0);
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.ForeColor = System.Drawing.Color.Silver;
            this.label18.Location = new System.Drawing.Point(3, 299);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(123, 23);
            this.label18.TabIndex = 17;
            this.label18.Text = "시작시간";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dateTimePicker_Start
            // 
            this.dateTimePicker_Start.CustomFormat = "HH시 mm분";
            this.dateTimePicker_Start.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_Start.Location = new System.Drawing.Point(132, 302);
            this.dateTimePicker_Start.Name = "dateTimePicker_Start";
            this.dateTimePicker_Start.ShowUpDown = true;
            this.dateTimePicker_Start.Size = new System.Drawing.Size(97, 21);
            this.dateTimePicker_Start.TabIndex = 17;
            this.dateTimePicker_Start.Value = new System.DateTime(2020, 10, 26, 9, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.Silver;
            this.label1.Location = new System.Drawing.Point(3, 253);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 23);
            this.label1.TabIndex = 25;
            this.label1.Text = "손절틱수";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDown_LoseTick
            // 
            this.numericUpDown_LoseTick.Location = new System.Drawing.Point(132, 256);
            this.numericUpDown_LoseTick.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_LoseTick.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_LoseTick.Name = "numericUpDown_LoseTick";
            this.numericUpDown_LoseTick.Size = new System.Drawing.Size(97, 21);
            this.numericUpDown_LoseTick.TabIndex = 23;
            this.numericUpDown_LoseTick.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.ForeColor = System.Drawing.Color.Silver;
            this.label8.Location = new System.Drawing.Point(3, 161);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(123, 23);
            this.label8.TabIndex = 26;
            this.label8.Text = "청산틱수";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDown_WinTick
            // 
            this.numericUpDown_WinTick.Location = new System.Drawing.Point(132, 164);
            this.numericUpDown_WinTick.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_WinTick.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_WinTick.Name = "numericUpDown_WinTick";
            this.numericUpDown_WinTick.Size = new System.Drawing.Size(97, 21);
            this.numericUpDown_WinTick.TabIndex = 23;
            this.numericUpDown_WinTick.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.ForeColor = System.Drawing.Color.Silver;
            this.label14.Location = new System.Drawing.Point(3, 138);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(123, 23);
            this.label14.TabIndex = 17;
            this.label14.Text = "청산 시간 단위(초)";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDown_CheckWinTerm
            // 
            this.numericUpDown_CheckWinTerm.Location = new System.Drawing.Point(132, 141);
            this.numericUpDown_CheckWinTerm.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_CheckWinTerm.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_CheckWinTerm.Name = "numericUpDown_CheckWinTerm";
            this.numericUpDown_CheckWinTerm.Size = new System.Drawing.Size(97, 21);
            this.numericUpDown_CheckWinTerm.TabIndex = 20;
            this.numericUpDown_CheckWinTerm.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.numericUpDown_SellRightCnt, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.button_AllClear, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.button_Stop, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.button_Start, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.button_BuyRight, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.button_SellRight, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.numericUpDown_BuyRightCnt, 1, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 248);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(232, 125);
            this.tableLayoutPanel3.TabIndex = 15;
            // 
            // numericUpDown_SellRightCnt
            // 
            this.numericUpDown_SellRightCnt.Location = new System.Drawing.Point(119, 65);
            this.numericUpDown_SellRightCnt.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_SellRightCnt.Name = "numericUpDown_SellRightCnt";
            this.numericUpDown_SellRightCnt.Size = new System.Drawing.Size(110, 21);
            this.numericUpDown_SellRightCnt.TabIndex = 19;
            this.numericUpDown_SellRightCnt.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // button_AllClear
            // 
            this.button_AllClear.BackColor = System.Drawing.Color.Transparent;
            this.button_AllClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_AllClear.ForeColor = System.Drawing.Color.Silver;
            this.button_AllClear.Location = new System.Drawing.Point(3, 96);
            this.button_AllClear.Name = "button_AllClear";
            this.button_AllClear.Size = new System.Drawing.Size(110, 25);
            this.button_AllClear.TabIndex = 17;
            this.button_AllClear.Text = "청산";
            this.button_AllClear.UseVisualStyleBackColor = false;
            this.button_AllClear.Click += new System.EventHandler(this.button_AllClear_Click);
            // 
            // button_Stop
            // 
            this.button_Stop.BackColor = System.Drawing.Color.Transparent;
            this.button_Stop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_Stop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Stop.ForeColor = System.Drawing.Color.Silver;
            this.button_Stop.Location = new System.Drawing.Point(119, 3);
            this.button_Stop.Name = "button_Stop";
            this.button_Stop.Size = new System.Drawing.Size(110, 25);
            this.button_Stop.TabIndex = 1;
            this.button_Stop.Text = "정지";
            this.button_Stop.UseVisualStyleBackColor = false;
            this.button_Stop.Click += new System.EventHandler(this.button_Stop_Click);
            // 
            // button_Start
            // 
            this.button_Start.BackColor = System.Drawing.Color.Transparent;
            this.button_Start.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_Start.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Start.ForeColor = System.Drawing.Color.Silver;
            this.button_Start.Location = new System.Drawing.Point(3, 3);
            this.button_Start.Name = "button_Start";
            this.button_Start.Size = new System.Drawing.Size(110, 25);
            this.button_Start.TabIndex = 0;
            this.button_Start.Text = "시작";
            this.button_Start.UseVisualStyleBackColor = false;
            this.button_Start.Click += new System.EventHandler(this.button_Start_Click);
            // 
            // button_BuyRight
            // 
            this.button_BuyRight.BackColor = System.Drawing.Color.Transparent;
            this.button_BuyRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_BuyRight.ForeColor = System.Drawing.Color.Silver;
            this.button_BuyRight.Location = new System.Drawing.Point(3, 34);
            this.button_BuyRight.Name = "button_BuyRight";
            this.button_BuyRight.Size = new System.Drawing.Size(110, 25);
            this.button_BuyRight.TabIndex = 2;
            this.button_BuyRight.Text = "매수";
            this.button_BuyRight.UseVisualStyleBackColor = false;
            this.button_BuyRight.Click += new System.EventHandler(this.button_BuyRight_Click);
            // 
            // button_SellRight
            // 
            this.button_SellRight.BackColor = System.Drawing.Color.Transparent;
            this.button_SellRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_SellRight.ForeColor = System.Drawing.Color.Silver;
            this.button_SellRight.Location = new System.Drawing.Point(3, 65);
            this.button_SellRight.Name = "button_SellRight";
            this.button_SellRight.Size = new System.Drawing.Size(110, 25);
            this.button_SellRight.TabIndex = 3;
            this.button_SellRight.Text = "매도";
            this.button_SellRight.UseVisualStyleBackColor = false;
            this.button_SellRight.Click += new System.EventHandler(this.button_SellRight_Click);
            // 
            // numericUpDown_BuyRightCnt
            // 
            this.numericUpDown_BuyRightCnt.Location = new System.Drawing.Point(119, 34);
            this.numericUpDown_BuyRightCnt.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_BuyRightCnt.Name = "numericUpDown_BuyRightCnt";
            this.numericUpDown_BuyRightCnt.Size = new System.Drawing.Size(110, 21);
            this.numericUpDown_BuyRightCnt.TabIndex = 18;
            this.numericUpDown_BuyRightCnt.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(28)))), ((int)(((byte)(37)))));
            this.panel2.Controls.Add(this.listBox_Price);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(232, 24);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(10, 850);
            this.panel2.TabIndex = 21;
            // 
            // listBox_Price
            // 
            this.listBox_Price.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox_Price.FormattingEnabled = true;
            this.listBox_Price.ItemHeight = 12;
            this.listBox_Price.Location = new System.Drawing.Point(0, 0);
            this.listBox_Price.Name = "listBox_Price";
            this.listBox_Price.Size = new System.Drawing.Size(10, 850);
            this.listBox_Price.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.panel3.Controls.Add(this.button_Test);
            this.panel3.Controls.Add(this.button_SaveLog);
            this.panel3.Controls.Add(this.button_ClearLog);
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.dataGridView_OderInfo);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.dataGridView_HavingStockInfo);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(242, 24);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(555, 850);
            this.panel3.TabIndex = 22;
            // 
            // button_Test
            // 
            this.button_Test.BackColor = System.Drawing.Color.Transparent;
            this.button_Test.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Test.ForeColor = System.Drawing.Color.Silver;
            this.button_Test.Location = new System.Drawing.Point(244, 313);
            this.button_Test.Name = "button_Test";
            this.button_Test.Size = new System.Drawing.Size(110, 25);
            this.button_Test.TabIndex = 31;
            this.button_Test.Text = "테스트";
            this.button_Test.UseVisualStyleBackColor = false;
            this.button_Test.Click += new System.EventHandler(this.button_Test_Click);
            // 
            // button_SaveLog
            // 
            this.button_SaveLog.BackColor = System.Drawing.Color.Transparent;
            this.button_SaveLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_SaveLog.ForeColor = System.Drawing.Color.Silver;
            this.button_SaveLog.Location = new System.Drawing.Point(128, 313);
            this.button_SaveLog.Name = "button_SaveLog";
            this.button_SaveLog.Size = new System.Drawing.Size(110, 25);
            this.button_SaveLog.TabIndex = 30;
            this.button_SaveLog.Text = "로그 저장";
            this.button_SaveLog.UseVisualStyleBackColor = false;
            this.button_SaveLog.Click += new System.EventHandler(this.button_SaveLog_Click);
            // 
            // button_ClearLog
            // 
            this.button_ClearLog.BackColor = System.Drawing.Color.Transparent;
            this.button_ClearLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_ClearLog.ForeColor = System.Drawing.Color.Silver;
            this.button_ClearLog.Location = new System.Drawing.Point(12, 313);
            this.button_ClearLog.Name = "button_ClearLog";
            this.button_ClearLog.Size = new System.Drawing.Size(110, 25);
            this.button_ClearLog.TabIndex = 29;
            this.button_ClearLog.Text = "로그 지우기";
            this.button_ClearLog.UseVisualStyleBackColor = false;
            this.button_ClearLog.Click += new System.EventHandler(this.button_ClearLog_Click);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.listBox_Log);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 344);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(555, 340);
            this.panel6.TabIndex = 27;
            // 
            // listBox_Log
            // 
            this.listBox_Log.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox_Log.FormattingEnabled = true;
            this.listBox_Log.ItemHeight = 12;
            this.listBox_Log.Location = new System.Drawing.Point(0, 0);
            this.listBox_Log.Name = "listBox_Log";
            this.listBox_Log.Size = new System.Drawing.Size(555, 340);
            this.listBox_Log.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.button_UpdateWin);
            this.panel4.Controls.Add(this.label_WinMoney);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.label_TodayWinMoney);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.label_HaveMoney);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Controls.Add(this.button_RemoveLog);
            this.panel4.Controls.Add(this.label_StandardPrice);
            this.panel4.Controls.Add(this.label_RemainTime);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(555, 28);
            this.panel4.TabIndex = 25;
            // 
            // button_UpdateWin
            // 
            this.button_UpdateWin.BackColor = System.Drawing.Color.Transparent;
            this.button_UpdateWin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_UpdateWin.ForeColor = System.Drawing.Color.Black;
            this.button_UpdateWin.Location = new System.Drawing.Point(455, 2);
            this.button_UpdateWin.Name = "button_UpdateWin";
            this.button_UpdateWin.Size = new System.Drawing.Size(97, 23);
            this.button_UpdateWin.TabIndex = 18;
            this.button_UpdateWin.Text = "갱신";
            this.button_UpdateWin.UseVisualStyleBackColor = false;
            this.button_UpdateWin.Visible = false;
            this.button_UpdateWin.Click += new System.EventHandler(this.button_UpdateWin_Click);
            // 
            // label_WinMoney
            // 
            this.label_WinMoney.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_WinMoney.AutoSize = true;
            this.label_WinMoney.BackColor = System.Drawing.Color.Transparent;
            this.label_WinMoney.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_WinMoney.ForeColor = System.Drawing.Color.Silver;
            this.label_WinMoney.Location = new System.Drawing.Point(389, 8);
            this.label_WinMoney.Name = "label_WinMoney";
            this.label_WinMoney.Size = new System.Drawing.Size(40, 12);
            this.label_WinMoney.TabIndex = 13;
            this.label_WinMoney.Text = "00000";
            this.label_WinMoney.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(338, 8);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(45, 12);
            this.label20.TabIndex = 12;
            this.label20.Text = "손익  : ";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_TodayWinMoney
            // 
            this.label_TodayWinMoney.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_TodayWinMoney.AutoSize = true;
            this.label_TodayWinMoney.BackColor = System.Drawing.Color.Transparent;
            this.label_TodayWinMoney.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_TodayWinMoney.ForeColor = System.Drawing.Color.Silver;
            this.label_TodayWinMoney.Location = new System.Drawing.Point(252, 8);
            this.label_TodayWinMoney.Name = "label_TodayWinMoney";
            this.label_TodayWinMoney.Size = new System.Drawing.Size(40, 12);
            this.label_TodayWinMoney.TabIndex = 11;
            this.label_TodayWinMoney.Text = "00000";
            this.label_TodayWinMoney.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(153, 8);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(93, 12);
            this.label17.TabIndex = 10;
            this.label17.Text = "당일매도손익  : ";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_HaveMoney
            // 
            this.label_HaveMoney.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_HaveMoney.AutoSize = true;
            this.label_HaveMoney.BackColor = System.Drawing.Color.Transparent;
            this.label_HaveMoney.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_HaveMoney.ForeColor = System.Drawing.Color.Black;
            this.label_HaveMoney.Location = new System.Drawing.Point(65, 8);
            this.label_HaveMoney.Name = "label_HaveMoney";
            this.label_HaveMoney.Size = new System.Drawing.Size(40, 12);
            this.label_HaveMoney.TabIndex = 9;
            this.label_HaveMoney.Text = "00000";
            this.label_HaveMoney.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(6, 8);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 12);
            this.label16.TabIndex = 8;
            this.label16.Text = "예탁금 : ";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_RemoveLog
            // 
            this.button_RemoveLog.Location = new System.Drawing.Point(827, 3);
            this.button_RemoveLog.Name = "button_RemoveLog";
            this.button_RemoveLog.Size = new System.Drawing.Size(101, 23);
            this.button_RemoveLog.TabIndex = 7;
            this.button_RemoveLog.Text = "로그 지우기";
            this.button_RemoveLog.UseVisualStyleBackColor = true;
            // 
            // label_StandardPrice
            // 
            this.label_StandardPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_StandardPrice.AutoSize = true;
            this.label_StandardPrice.BackColor = System.Drawing.Color.Transparent;
            this.label_StandardPrice.ForeColor = System.Drawing.Color.Silver;
            this.label_StandardPrice.Location = new System.Drawing.Point(621, 8);
            this.label_StandardPrice.Name = "label_StandardPrice";
            this.label_StandardPrice.Size = new System.Drawing.Size(45, 12);
            this.label_StandardPrice.TabIndex = 6;
            this.label_StandardPrice.Text = "..........";
            this.label_StandardPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_RemainTime
            // 
            this.label_RemainTime.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_RemainTime.AutoSize = true;
            this.label_RemainTime.BackColor = System.Drawing.Color.Transparent;
            this.label_RemainTime.ForeColor = System.Drawing.Color.Silver;
            this.label_RemainTime.Location = new System.Drawing.Point(552, 8);
            this.label_RemainTime.Name = "label_RemainTime";
            this.label_RemainTime.Size = new System.Drawing.Size(49, 12);
            this.label_RemainTime.TabIndex = 5;
            this.label_RemainTime.Text = "...........";
            this.label_RemainTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dataGridView_OderInfo
            // 
            this.dataGridView_OderInfo.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView_OderInfo.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.ButtonShadow;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_OderInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView_OderInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_OderInfo.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.dataGridView_OderInfo.Location = new System.Drawing.Point(12, 193);
            this.dataGridView_OderInfo.Name = "dataGridView_OderInfo";
            this.dataGridView_OderInfo.ReadOnly = true;
            this.dataGridView_OderInfo.RowHeadersVisible = false;
            this.dataGridView_OderInfo.RowTemplate.Height = 23;
            this.dataGridView_OderInfo.Size = new System.Drawing.Size(531, 114);
            this.dataGridView_OderInfo.TabIndex = 24;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Silver;
            this.label10.Location = new System.Drawing.Point(10, 173);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(89, 12);
            this.label10.TabIndex = 23;
            this.label10.Text = "== 주문 현황 ==";
            // 
            // dataGridView_HavingStockInfo
            // 
            this.dataGridView_HavingStockInfo.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView_HavingStockInfo.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.ButtonShadow;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_HavingStockInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView_HavingStockInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_HavingStockInfo.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.dataGridView_HavingStockInfo.Location = new System.Drawing.Point(12, 57);
            this.dataGridView_HavingStockInfo.Name = "dataGridView_HavingStockInfo";
            this.dataGridView_HavingStockInfo.ReadOnly = true;
            this.dataGridView_HavingStockInfo.RowHeadersVisible = false;
            this.dataGridView_HavingStockInfo.RowTemplate.Height = 23;
            this.dataGridView_HavingStockInfo.Size = new System.Drawing.Size(531, 108);
            this.dataGridView_HavingStockInfo.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Silver;
            this.label9.Location = new System.Drawing.Point(12, 38);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 12);
            this.label9.TabIndex = 21;
            this.label9.Text = "== 잔고 현황 ==";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.chart_Price);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 684);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(555, 166);
            this.panel5.TabIndex = 32;
            // 
            // chart_Price
            // 
            chartArea1.Name = "ChartArea1";
            this.chart_Price.ChartAreas.Add(chartArea1);
            this.chart_Price.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.chart_Price.Legends.Add(legend1);
            this.chart_Price.Location = new System.Drawing.Point(0, 0);
            this.chart_Price.Name = "chart_Price";
            this.chart_Price.Size = new System.Drawing.Size(555, 166);
            this.chart_Price.TabIndex = 0;
            this.chart_Price.Text = "chart1";
            this.chart_Price.MouseMove += new System.Windows.Forms.MouseEventHandler(this.chart_Price_MouseMove);
            // 
            // axKHOpenAPI1
            // 
            this.axKHOpenAPI1.Enabled = true;
            this.axKHOpenAPI1.Location = new System.Drawing.Point(1021, 454);
            this.axKHOpenAPI1.Name = "axKHOpenAPI1";
            this.axKHOpenAPI1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axKHOpenAPI1.OcxState")));
            this.axKHOpenAPI1.Size = new System.Drawing.Size(100, 50);
            this.axKHOpenAPI1.TabIndex = 13;
            this.axKHOpenAPI1.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 874);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.axKHOpenAPI1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "선물 트레이더";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel_Option.ResumeLayout(false);
            this.tableLayoutPanel_Option.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_TodayWinMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Term)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_PerCnt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_GoalTick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_LimitOrderingTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_LoseTick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_WinTick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_CheckWinTerm)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SellRightCnt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BuyRightCnt)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_OderInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_HavingStockInfo)).EndInit();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart_Price)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axKHOpenAPI1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label_Server;
        private System.Windows.Forms.Label label_UserID;
        private System.Windows.Forms.Label label_UserName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loginToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label_KospiPrice;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox_KospiCode;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox_Account;
        private AxKHOpenAPILib.AxKHOpenAPI axKHOpenAPI1;
        private System.Windows.Forms.Button button_CheckAcc;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button button_Stop;
        private System.Windows.Forms.Button button_Start;
        private System.Windows.Forms.Button button_TraceStock;
        private System.Windows.Forms.DataGridView dataGridView_OderInfo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dataGridView_HavingStockInfo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_Option;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button button_AllClear;
        private System.Windows.Forms.Button button_BuyRight;
        private System.Windows.Forms.Button button_SellRight;
        private System.Windows.Forms.NumericUpDown numericUpDown_LimitOrderingTime;
        private System.Windows.Forms.NumericUpDown numericUpDown_Term;
        private System.Windows.Forms.NumericUpDown numericUpDown_PerCnt;
        private System.Windows.Forms.NumericUpDown numericUpDown_SellRightCnt;
        private System.Windows.Forms.NumericUpDown numericUpDown_BuyRightCnt;
        private System.Windows.Forms.NumericUpDown numericUpDown_GoalTick;
        private System.Windows.Forms.CheckBox checkBox_BuyRight;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label_RemainTime;
        private System.Windows.Forms.ListBox listBox_Log;
        private System.Windows.Forms.Label label_StandardPrice;
        private System.Windows.Forms.CheckBox checkBox_SellRight;
        private System.Windows.Forms.Button button_RemoveLog;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.NumericUpDown numericUpDown_LoseTick;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown_WinTick;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button_ClearLog;
        private System.Windows.Forms.Button button_SaveLog;
        private System.Windows.Forms.NumericUpDown numericUpDown_CheckWinTerm;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker dateTimePicker_End;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DateTimePicker dateTimePicker_Start;
        private System.Windows.Forms.Button button_Test;
        private System.Windows.Forms.CheckBox checkBox_ClearOption;
        private System.Windows.Forms.Label label_WinMoney;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label_TodayWinMoney;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label_HaveMoney;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_Price;
        private System.Windows.Forms.ListBox listBox_Price;
        private System.Windows.Forms.Button button_UpdateWin;
        private System.Windows.Forms.Label label_ClearDiff;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label_PurchaseDiff;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label_LoseDiff;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.NumericUpDown numericUpDown_TodayWinMoney;
        private System.Windows.Forms.CheckBox checkBox_TodayWinMoney;
    }
}

