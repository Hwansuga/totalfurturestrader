﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using AxKHOpenAPILib;

namespace totalfurturestrader
{
    public partial class LogViewer : Form
    {
        public LogViewer()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | Global.CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        private void LogViewer_Load(object sender, EventArgs e)
        {

        }

        public void Print_CommonLog(string strMsg_)
        {
            listBox_CommonLog.Invoke((MethodInvoker)delegate
            {
                Util.AutoLineStringAdd(listBox_CommonLog, strMsg_);
            });

        }

        public void Print_OrderLog(string strMsg_)
        {
//             listBox_OrderLog.Invoke((MethodInvoker)delegate
//             {
//                 Util.AutoLineStringAdd(listBox_OrderLog, strMsg_);
//             });
        }

        public void Print_RealTimeLog(string strMsg_)
        {
            listBox_RealTime.Invoke((MethodInvoker)delegate
            {
                Util.AutoLineStringAdd(listBox_RealTime, strMsg_);
            });
        }

        public void OnEventConnect(object sender, _DKHOpenAPIEvents_OnEventConnectEvent e)
        {
            string data_ = "";
            try
            {
                data_ = LitJson.JsonMapper.ToJson(e);
            }
            catch { }
            
            Print_CommonLog("◀OnEventConnect << nErrCode : " + e.nErrCode + " / " + data_);
        }

        public void OnReceiveConditionVer(object sender, _DKHOpenAPIEvents_OnReceiveConditionVerEvent e)
        {
            string data_ = "";
            try
            {
                data_ = LitJson.JsonMapper.ToJson(e);
            }
            catch { }

            Print_CommonLog("◀OnReceiveConditionVer << lRet : " + e.lRet + " / " + data_);
        }

        public void OnReceiveTrCondition(object sender, _DKHOpenAPIEvents_OnReceiveTrConditionEvent e)
        {
            string data_ = "";
            try
            {
                data_ = LitJson.JsonMapper.ToJson(e);
            }
            catch { }

            Print_CommonLog("◀OnReceiveTrCondition << " + data_);
        }

        public void OnReceiveRealCondition(object sender, _DKHOpenAPIEvents_OnReceiveRealConditionEvent e)
        {
            string data_ = "";
            try
            {
                data_ = LitJson.JsonMapper.ToJson(e);
            }
            catch { }

            Print_CommonLog("◀OnReceiveRealCondition << sTrCode : " + e.sTrCode + " / " + data_);
        }

        public void OnReceiveTrData(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveTrDataEvent e)
        {
            string data_ = "";
            try
            {
                data_ = LitJson.JsonMapper.ToJson(e);
            }
            catch { }

            Print_CommonLog($"◀OnReceiveTrData {KiwoomManager.Instance.GetScreenName(int.Parse(e.sScrNo))}<< sErrorCode : {e.sErrorCode} / {data_}");
        }

        public void OnReceiveRealData(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveRealDataEvent e)
        {
            string data_ = "";
            try
            {
                data_ = LitJson.JsonMapper.ToJson(e);
            }
            catch { }

            Print_RealTimeLog($"◀OnReceiveRealData << {data_}");
        }

        public void OnReceiveChejanData(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveChejanDataEvent e)
        {
            string data_ = "";
            try
            {
                data_ = LitJson.JsonMapper.ToJson(e);
            }
            catch { }

            Print_CommonLog($"◀OnReceiveChejanData << {data_}");
        }

        public void OnReceiveMsg(object sender, _DKHOpenAPIEvents_OnReceiveMsgEvent e)
        {
            string data_ = "";
            try
            {
                data_ = LitJson.JsonMapper.ToJson(e);
            }
            catch { }

            Print_CommonLog($"◀OnReceiveMsg {KiwoomManager.Instance.GetScreenName(int.Parse(e.sScrNo))}<< {data_}");
        }

        public void SaveLog()
        {
            Util.SaveTxtFile(DateTime.Now.ToString("yyyyMMdd_HH-mm_") + "CommonLog.txt", listBox_CommonLog.Items.Cast<string>().ToList());
            //Util.SaveTxtFile(DateTime.Now.ToString("yyyyMMdd_HH-mm_") + "OrderLog.txt", listBox_OrderLog.Items.Cast<string>().ToList());
        }
    }
}
