﻿namespace totalfurturestrader
{
    partial class LogViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogViewer));
            this.listBox_RealTime = new System.Windows.Forms.ListBox();
            this.listBox_CommonLog = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // listBox_RealTime
            // 
            this.listBox_RealTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox_RealTime.FormattingEnabled = true;
            this.listBox_RealTime.ItemHeight = 12;
            this.listBox_RealTime.Location = new System.Drawing.Point(12, 540);
            this.listBox_RealTime.Name = "listBox_RealTime";
            this.listBox_RealTime.Size = new System.Drawing.Size(564, 112);
            this.listBox_RealTime.TabIndex = 7;
            // 
            // listBox_CommonLog
            // 
            this.listBox_CommonLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox_CommonLog.FormattingEnabled = true;
            this.listBox_CommonLog.ItemHeight = 12;
            this.listBox_CommonLog.Location = new System.Drawing.Point(12, 12);
            this.listBox_CommonLog.Name = "listBox_CommonLog";
            this.listBox_CommonLog.Size = new System.Drawing.Size(564, 520);
            this.listBox_CommonLog.TabIndex = 5;
            // 
            // LogViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.ClientSize = new System.Drawing.Size(588, 664);
            this.Controls.Add(this.listBox_RealTime);
            this.Controls.Add(this.listBox_CommonLog);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LogViewer";
            this.Text = "LogViewer";
            this.Load += new System.EventHandler(this.LogViewer_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBox_RealTime;
        private System.Windows.Forms.ListBox listBox_CommonLog;
    }
}