﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

class ProcData
{
    public Action<object> startAction;
    public object param;
    public string name;

    public void DoAction() { startAction(param); }
}

class ProcScript : Singleton<ProcScript>
{
    public ProcData curProc= null;
    public List<ProcData> listProcData = new List<ProcData>();

    public Action additionEvent = null;

    public void AddProc(string name_, Action<object> startAction_ , object param_)
    {
        ProcData procData = new ProcData
        {
            name = name_,
            startAction = startAction_,
            param = param_,
        };

        listProcData.Add(procData);
        NextProc();
    }

    public void AddProcFront(string name_ , Action<object> startAction_, object param_)
    {
        ProcData procData = new ProcData
        {
            startAction = startAction_,
            param = param_,
        };
        listProcData.Insert(0,procData);
        NextProc();
    }

    void NextProc()
    {
        if (curProc != null)
            return;

        if (listProcData.Count <= 0)
            return;

        curProc = listProcData[0];
        listProcData.RemoveAt(0);

        curProc.DoAction();

        if (additionEvent != null)
            additionEvent();
    }

    public void DoneProc()
    {
        curProc = null;
        NextProc();
    }

    public void DeleteProc(string actionName_)
    {
        listProcData.RemoveAll(x => x.startAction.Method.Name == actionName_);
        if (curProc != null && curProc.startAction.Method.Name == actionName_)
            DoneProc();
    }

    public void DeleteAllProc()
    {
        listProcData.Clear();
        DoneProc();
    }

    public List<string> GetJobsName()
    {
        List<string> list = new List<string>();
        if (curProc != null)
        {
            list.Add(curProc.name);
            foreach (var item in listProcData)
                list.Add(item.name);
        }

        return list;
    }

    public bool IsEmpty()
    {
        return curProc == null && listProcData.Count <= 0;
    }
}

