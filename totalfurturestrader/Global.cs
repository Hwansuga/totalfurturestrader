﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using totalfurturestrader;

class PriceEntityObject
{
    public string date { get; set; }
    public double open { get; set; }
    public double high { get; set; }
    public double low { get; set; }
    public double close { get; set; }
}

class JobInfo
{
    public string dataID { get; set; }
    public string dataInfo { get; set; }
}

public class StockInfo
{ 
    public string 종목코드 { get; set; }
    public string 종목명 { get; set; } 
    public string 매매구분 { get; set; }
    public string 보유수량 { get; set; }
    public string 매입단가 { get; set; }
}

public class OrderProcessInfo
{
    public string 종목코드 { get; set; }
    public string 주문구분 { get; set; }
    public string 주문수량 { get; set; }
    public string 주문번호 { get; set; }
    public string 주문가격 { get; set; }
}

public class Setting
{
    public int numericUpDown_BuyRightCnt;
    public int numericUpDown_SellRightCnt;

    public int numericUpDown_PerCnt;
    public int numericUpDown_Term;
    public int numericUpDown_GoalTick;
    public int numericUpDown_LimitOrderingTime;
    public bool checkBox_BuyRight;
    public bool checkBox_SellRight;

    public int numericUpDown_CheckWinTerm;
    public int numericUpDown_WinTick;
    public bool checkBox_ClearOption;

    public int numericUpDown_LoseTick;

    public bool checkBox_TodayWinMoney;
    public int numericUpDown_TodayWinMoney;
}

class Global : Singleton<Global>
{
    public static bool stop = true;

    public static Form1 uiController = null;
    public static LogViewer logViewer = null;

    public const int CP_NOCLOSE_BUTTON = 0x200;
    public const float oneDigit = 0.05f;
    public const long valuePerDigit = 12500;

    public static double curStockPrice = 0.0f;
    public static double standardPrice = 0.0f;
    public static double purchasePrice = 0.0f;
    public static double curTodayWinmoney = 0.0f;

    public static Dictionary<string, double> dicHistoryPrice = new Dictionary<string, double>();
    public static List<PriceEntityObject> listHistoryPrice = new List<PriceEntityObject>();

    public static string selectedAccNo = "";
   
    public static List<StockInfo> listStockInfo = new List<StockInfo>();
    public static List<OrderProcessInfo> listOrderIfo = new List<OrderProcessInfo>();

    public static string comboBox_Account;
    public static string comboBox_KospiCode;

    public static int numericUpDown_BuyRightCnt;
    public static int numericUpDown_SellRightCnt;

    public static int numericUpDown_PerCnt;
    public static int numericUpDown_Term;
    public static int numericUpDown_GoalTick;
    public static int numericUpDown_LimitOrderingTime;
    public static bool checkBox_BuyRight;
    public static bool checkBox_SellRight;

    
    public static int numericUpDown_CheckWinTerm;
    public static int numericUpDown_WinTick;
    public static bool checkBox_ClearOption;

    public static int numericUpDown_LoseTick;

    public static bool checkBox_TodayWinMoney;
    public static int numericUpDown_TodayWinMoney;

    public static void UpdateValueFrom(object uiContorller_)
    {
        List<FieldInfo> listProperty = uiContorller_.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).ToList();
        List<FieldInfo> listGlobalProperty = Global.Instance.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).ToList();

        List<FieldInfo> listNumericUpDown = listProperty.FindAll(x => x.FieldType == typeof(NumericUpDown));
        foreach (var item in listNumericUpDown)
        {
            NumericUpDown temp = (NumericUpDown)item.GetValue(uiContorller_);
            Util.SetValue(Global.Instance, temp.Name, temp.Value);
        }

        List<FieldInfo> listCombobox = listProperty.FindAll(x => x.FieldType == typeof(ComboBox));
        foreach (var item in listCombobox)
        {
            ComboBox temp = (ComboBox)item.GetValue(uiContorller_);
            Util.SetValue(Global.Instance, temp.Name, temp.SelectedItem);
        }

        List<FieldInfo> listCheckBox = listProperty.FindAll(x => x.FieldType == typeof(CheckBox));
        foreach (FieldInfo item in listCheckBox)
        {
            CheckBox temp = (CheckBox)item.GetValue(uiContorller_);
            Util.SetValue(Global.Instance, temp.Name, temp.Checked);
        }
    }

    static Dictionary<string, double> GetData(int startIndex, int endIndex)
    {
        return dicHistoryPrice.OrderBy(d => d.Key).Skip(startIndex).Take(endIndex - startIndex + 1).ToDictionary(k => k.Key, v => v.Value);
    }

    public static bool AddHistoryData(string key_ , double value_)
    {
        var entity = listHistoryPrice.Find(x => x.date == key_);
        if (entity == null)
        {
//             if (listHistoryPrice.Count > 0)
//             {
//                 var last = listHistoryPrice.Last();
//                 uiController.UpdatePrice($"{last.date} : {last.low} ~ {last.high}");
//             }

            var temp = new PriceEntityObject();
            temp.date = key_;
            temp.close = temp.low = temp.high = temp.open = value_;
            listHistoryPrice.Add(temp);
        }
        else
        {
            entity.close = value_;
            if (entity.close < entity.low)
                entity.low = entity.close;

            if (entity.close > entity.high)
                entity.high = entity.close;
        }

        if (listHistoryPrice.Count > 20)
        {
            listHistoryPrice = listHistoryPrice.GetRange(listHistoryPrice.Count - 20, 20);
        }

        if (dicHistoryPrice.ContainsKey(key_) == false)
        {
            double diff = 0f;
            if (dicHistoryPrice.Count > 0)
                diff = Math.Abs(dicHistoryPrice.Last().Value - value_);
            dicHistoryPrice.Add(key_, value_);
            if (dicHistoryPrice.Count > 100)
            {
                dicHistoryPrice = GetData(dicHistoryPrice.Count - numericUpDown_Term + 2 , dicHistoryPrice.Count);
            }

            if (diff > 0)
                return true;
        }
        else
        {
            if (dicHistoryPrice[key_] != value_)
            {
                dicHistoryPrice[key_] = value_;
                return true;
            }
        }
        return false;
    }

    public static double GetMaxValueInTerm(int term_)
    {
        int cnt = 0;
        double max = 0;
        foreach(var item in dicHistoryPrice)
        {
            if (cnt >= term_)
                break;

            if (item.Value > max)
                max = item.Value;
        }

        return max;
    }

    public static double GetMinValueInTerm(int term_)
    {
        int cnt = 0;
        double min = 100000;
        foreach (var item in dicHistoryPrice)
        {
            if (cnt >= term_)
                break;

            if (item.Value < min)
                min = item.Value;
        }

        return min;
    }
}

